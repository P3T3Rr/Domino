
import javax.swing.ImageIcon;
import javax.swing.JFrame;


public class UsuarioConfirmar2 extends javax.swing.JFrame {
    Login login;
    Metodos met;
    UsuarioNuevoJuego NJ;
    
    public UsuarioConfirmar2(Login login,  Metodos met, UsuarioNuevoJuego NJ) {
        this.login = login;
        this.met = met;
        this.NJ = NJ;
        this.setTitle("Contraseña del Oponente");
        this.setResizable(false);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/descarga.png")).getImage());
        initComponents();
        nombre1.setText(NJ.OponentesSeleccionados.get(0).nombre);
        nombre2.setText(NJ.OponentesSeleccionados.get(1).nombre);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        Contrasena2 = new javax.swing.JPasswordField();
        Contrasena1 = new javax.swing.JPasswordField();
        jLabel1 = new javax.swing.JLabel();
        Jugar = new javax.swing.JButton();
        Jugar1 = new javax.swing.JButton();
        nombre1 = new javax.swing.JLabel();
        nombre2 = new javax.swing.JLabel();
        check1 = new javax.swing.JLabel();
        check2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        jButton1.setText("jButton1");

        jLabel2.setText("jLabel2");

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setLocation(new java.awt.Point(710, 280));
        setMaximumSize(new java.awt.Dimension(670, 500));
        setMinimumSize(new java.awt.Dimension(670, 500));
        setPreferredSize(new java.awt.Dimension(670, 500));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(null);

        Contrasena2.setBackground(new java.awt.Color(51, 51, 51));
        Contrasena2.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Contrasena2.setForeground(new java.awt.Color(204, 204, 204));
        Contrasena2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        Contrasena2.setToolTipText("contraseña");
        getContentPane().add(Contrasena2);
        Contrasena2.setBounds(380, 220, 190, 40);

        Contrasena1.setBackground(new java.awt.Color(51, 51, 51));
        Contrasena1.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Contrasena1.setForeground(new java.awt.Color(204, 204, 204));
        Contrasena1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        Contrasena1.setToolTipText("contraseña");
        getContentPane().add(Contrasena1);
        Contrasena1.setBounds(100, 220, 190, 40);

        jLabel1.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 204, 204));
        jLabel1.setText("Contraseña de los Oponentes");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(50, 60, 590, 60);

        Jugar.setBackground(new java.awt.Color(0, 0, 0));
        Jugar.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        Jugar.setForeground(new java.awt.Color(51, 255, 51));
        Jugar.setText("Aceptar");
        Jugar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JugarActionPerformed(evt);
            }
        });
        getContentPane().add(Jugar);
        Jugar.setBounds(370, 340, 210, 60);

        Jugar1.setBackground(new java.awt.Color(0, 0, 0));
        Jugar1.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        Jugar1.setForeground(new java.awt.Color(166, 31, 31));
        Jugar1.setText("Cancelar");
        Jugar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jugar1ActionPerformed(evt);
            }
        });
        getContentPane().add(Jugar1);
        Jugar1.setBounds(90, 340, 210, 60);

        nombre1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        nombre1.setForeground(new java.awt.Color(204, 204, 204));
        nombre1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        nombre1.setText("Nombre");
        getContentPane().add(nombre1);
        nombre1.setBounds(130, 170, 130, 30);

        nombre2.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        nombre2.setForeground(new java.awt.Color(204, 204, 204));
        nombre2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        nombre2.setText("Nombre");
        getContentPane().add(nombre2);
        nombre2.setBounds(410, 170, 130, 30);
        getContentPane().add(check1);
        check1.setBounds(300, 220, 41, 40);
        getContentPane().add(check2);
        check2.setBounds(580, 220, 41, 40);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fonde_Juego_opt - copia.jpg"))); // NOI18N
        jLabel4.setMaximumSize(new java.awt.Dimension(600, 500));
        jLabel4.setMinimumSize(new java.awt.Dimension(600, 500));
        jLabel4.setPreferredSize(new java.awt.Dimension(600, 500));
        getContentPane().add(jLabel4);
        jLabel4.setBounds(0, 0, 680, 480);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void JugarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JugarActionPerformed
        String contrasena1 = "";
        char []  contra1 = Contrasena1.getPassword();
        for(int i=0; i<contra1.length; i++) contrasena1 += contra1[i];
        
        String contrasena2 = "";
        char []  contra2 = Contrasena2.getPassword();
        for(int i=0; i<contra2.length; i++) contrasena2 += contra2[i];
        
        boolean x = met.InicioDeSesion(NJ.OponentesSeleccionados.get(0).cedula, contrasena1);
        boolean y = met.InicioDeSesion(NJ.OponentesSeleccionados.get(1).cedula, contrasena2);
        
        if (x == true){
            ImageIcon icon = new ImageIcon(this.getClass().getResource("/Imagenes/bueno_opt.png"));
            check1.setIcon(icon);
        }
        else{
            ImageIcon icon = new ImageIcon(this.getClass().getResource("/Imagenes/bmalo_opt.png"));
            check1.setIcon(icon);
        }
        
        if (y == true){
            ImageIcon icon = new ImageIcon(this.getClass().getResource("/Imagenes/bueno_opt.png"));
            check2.setIcon(icon);
        }
        else{
            ImageIcon icon = new ImageIcon(this.getClass().getResource("/Imagenes/bmalo_opt.png"));
            check2.setIcon(icon);
        }
        
        if(x == true && y == true){
            this.dispose();
            met.CrearFichas();
            Juego2 jugar = new Juego2(met, login);

            JFrame frameJuego = new JFrame();
            frameJuego.setLocation(10, 10);
            frameJuego.setSize(1900,1050);
            frameJuego.setTitle("Juego 1 vs 1");
            frameJuego.setResizable(false);
            frameJuego.add(jugar);
            frameJuego.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/descarga.png")).getImage());
            frameJuego.setVisible(true);
        }
    }//GEN-LAST:event_JugarActionPerformed

    private void Jugar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jugar1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_Jugar1ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        this.dispose();
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField Contrasena1;
    private javax.swing.JPasswordField Contrasena2;
    private javax.swing.JButton Jugar;
    private javax.swing.JButton Jugar1;
    private javax.swing.JLabel check1;
    private javax.swing.JLabel check2;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel nombre1;
    private javax.swing.JLabel nombre2;
    // End of variables declaration//GEN-END:variables
}
