
import java.awt.Image;
import java.io.Serializable;
import java.util.ArrayList;


public class Partida implements Serializable{
    int jugadores;
    Image captura;
    Ficha seleccionada;
    ArrayList<Ficha> misFichas;
    ArrayList<Ficha> fichasOponente;
    ArrayList<Ficha> fichasOponente2;
    ArrayList<Ficha> fichasOponente3;
    Ficha iniciojuego;
    Ficha finJuego;
    int JuegoYo;
    boolean trampa1;
    boolean trampa2;
    boolean trampa3;
    boolean trampa4;
    int Vuelta;
    
    Partida sig, ant;

    public Partida( Ficha seleccionada, ArrayList<Ficha> misFichas, ArrayList<Ficha> fichasOponente, Ficha iniciojuego, Ficha finJuego, int JuegoYo, boolean trampa1, boolean trampa2, int Vuelta) {
        this.jugadores = 2;
        this.seleccionada = seleccionada;
        this.misFichas = misFichas;
        this.fichasOponente = fichasOponente;
        this.iniciojuego = iniciojuego;
        this.finJuego = finJuego;
        this.JuegoYo = JuegoYo;
        this.trampa1 = trampa1;
        this.trampa2 = trampa2;
        this.Vuelta = Vuelta;
    }

    public Partida( Ficha seleccionada, ArrayList<Ficha> misFichas, ArrayList<Ficha> fichasOponente, ArrayList<Ficha> fichasOponente2, Ficha iniciojuego, Ficha finJuego, int JuegoYo, boolean trampa1, boolean trampa2, boolean trampa3, int Vuelta) {
        this.jugadores = 3;
        this.seleccionada = seleccionada;
        this.misFichas = misFichas;
        this.fichasOponente = fichasOponente;
        this.fichasOponente2 = fichasOponente2;
        this.iniciojuego = iniciojuego;
        this.finJuego = finJuego;
        this.JuegoYo = JuegoYo;
        this.trampa1 = trampa1;
        this.trampa2 = trampa2;
        this.trampa3 = trampa3;
        this.Vuelta = Vuelta;
    }

    public Partida( Ficha seleccionada, ArrayList<Ficha> misFichas, ArrayList<Ficha> fichasOponente, ArrayList<Ficha> fichasOponente2, ArrayList<Ficha> fichasOponente3, Ficha iniciojuego, Ficha finJuego, int JuegoYo, boolean trampa1, boolean trampa2, boolean trampa3, boolean trampa4, int Vuelta) {
        this.jugadores = 4;
        this.seleccionada = seleccionada;
        this.misFichas = misFichas;
        this.fichasOponente = fichasOponente;
        this.fichasOponente2 = fichasOponente2;
        this.fichasOponente3 = fichasOponente3;
        this.iniciojuego = iniciojuego;
        this.finJuego = finJuego;
        this.JuegoYo = JuegoYo;
        this.trampa1 = trampa1;
        this.trampa2 = trampa2;
        this.trampa3 = trampa3;
        this.trampa4 = trampa4;
        this.Vuelta = Vuelta;
    }
    
}
