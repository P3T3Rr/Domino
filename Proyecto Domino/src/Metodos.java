
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;


public class Metodos {
     public Usuario inicio, fin;
     public Ficha primeraFicha, ultimaFicha;
     
     //__________________________________________METODOS DEL USUARIO__________________________________________
     
    public Usuario buscarUsuario(String cedula) {
        if (inicio == null)
            return null;
        Usuario pos = inicio;
        while (pos != fin) {
            if (pos.cedula.equals(cedula))
                return pos;
            pos = pos.sig;
        }
        if (pos.cedula.equals(cedula))
            return fin;
        else
            return null;
    }
    
    public boolean registrarse(String nombre, String cedula, String contrasena) {
        if (buscarUsuario(cedula) == null) {
            Usuario nuevo = new Usuario(nombre, cedula, contrasena);
            if (inicio == null) {
                inicio = fin = nuevo;
                inicio.ant = fin;
                inicio.sig = inicio;
                guardar();
                return true;
            }
            inicio.ant = nuevo;   
            nuevo.sig = inicio;
            inicio = nuevo;
            inicio.ant = fin;
            fin.sig = inicio;
            guardar();
            return true;
        }
        return false;
    }
    
    public boolean InicioDeSesion(String cedula, String contrasena) {
        Usuario usuario = buscarUsuario(cedula);
        if (usuario == null) {
            JOptionPane.showMessageDialog(null, "El usuario no existe...!!");
        } else {
            if (usuario.contrasena.equals(contrasena)) {
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Contraseña incorrecta...!!");
            }
        }
        return false;
    }
    
    public void editarCuenta(String Cedula, String nombre, String contrasena, String cedula) {
        Usuario usuario = buscarUsuario(Cedula);
        if (usuario != null){
            Usuario aux = inicio;
            while (!aux.equals(usuario)) {
                aux = aux.sig;
            }
            aux.nombre = nombre;
            aux.cedula = cedula;
            aux.contrasena = contrasena;

            JOptionPane.showMessageDialog(null, "Cuenta Editada...!!");
            guardar();
        }
        else{
            JOptionPane.showMessageDialog(null, "No existe un usuario con la cédula ("+Cedula+")...!!");
        }
    }
    
    public void eliminarCuenta(String cedula) {
        Usuario usuario = buscarUsuario(cedula);
        if (usuario != null){
            Usuario aux = inicio;
            while (!aux.equals(usuario)) {
                aux = aux.sig;
            }
            aux.sig.ant = aux.ant;
            aux.ant.sig = aux.sig;

            JOptionPane.showMessageDialog(null, "Cuenta Eliminada...!!");
            guardar();
        }
        else
            JOptionPane.showMessageDialog(null, "No existe un usuario con la cédula ("+cedula+")...!!");
    }
        
    //____________________________________________GUARDAR Y LEER________________________________________________
    
    public void guardar() {
        File ruta_archivo = new File("src/archivo.dat");
        Usuario usuario = inicio;
        if (ruta_archivo.exists()) {
            ruta_archivo.delete();
        }
        try {
            ObjectOutputStream archivo = new ObjectOutputStream(new FileOutputStream(ruta_archivo));
            if (inicio != fin){
                archivo.writeObject(usuario);
                System.out.println("Usuario guardado " + usuario.nombre);
                usuario = usuario.sig;
                while (usuario != inicio) {
                    archivo.writeObject(usuario);
                    System.out.println("Usuario guardado " + usuario.nombre);
                    usuario = usuario.sig;
                }
            }
            else{
                archivo.writeObject(usuario);
                System.out.println("Usuario guardado " + usuario.nombre);
            }
            
        } catch (IOException i) {
            System.out.println("El usuario no se guardo" + i);

        }
    }

    public void leer() throws FileNotFoundException {
        String ruta_archivo = "src/archivo.dat";
        try {
            ObjectInputStream archivo = new ObjectInputStream(new FileInputStream(ruta_archivo));
            Usuario us = (Usuario) archivo.readObject();
            
            System.out.println("Usuario leido " + us.nombre);
            registrarse(us.nombre, us.cedula, us.contrasena);
            us = us.sig;
            
            while (!us.cedula.equals(fin.cedula)) {
                System.out.println("Usuario leido " + us.nombre);
                registrarse(us.nombre, us.cedula, us.contrasena);
                if (us.InicioPartida != null){
                    System.out.println("P///// ");
                    Partida aux = us.InicioPartida;
                    while (aux != null){
                        switch (aux.jugadores) {
                            case 2:{
                                Partida nueva = new Partida(aux.seleccionada, aux.misFichas, aux.fichasOponente, aux.iniciojuego, aux.finJuego, aux.JuegoYo, aux.trampa1, aux.trampa2, aux.Vuelta);
                                us.setPartida(nueva);
                                break;
                            }
                            case 3:{
                                Partida nueva = new Partida(aux.seleccionada, aux.misFichas, aux.fichasOponente, aux.fichasOponente2, aux.iniciojuego, aux.finJuego, aux.JuegoYo, aux.trampa1, aux.trampa2, aux.trampa3, aux.Vuelta);
                                us.setPartida(nueva);
                                break;
                            }
                            default:{
                                Partida nueva = new Partida(aux.seleccionada, aux.misFichas, aux.fichasOponente, aux.fichasOponente2, aux.fichasOponente3, aux.iniciojuego, aux.finJuego, aux.JuegoYo, aux.trampa1, aux.trampa2, aux.trampa3, aux.trampa4, aux.Vuelta);
                                us.setPartida(nueva);
                                break;
                            }
                        }
                        System.out.println("Partida leida ");
                        aux = aux.sig;
                    }
                }
                us = us.sig;
            }
            guardar();
            
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("El usuario no se a leido" + ex);
        }
    }
    
    //___________________________________________METODOS DE FICHAS______________________________________________

    public void agregarFicha(Ficha nuevo){
        if (primeraFicha == null) 
            primeraFicha = ultimaFicha = nuevo;
        
        else{ 
            ultimaFicha.sig = nuevo;
            ultimaFicha = nuevo;
        }
    }
    
    public void eliminarFicha(Ficha ficha){
        Ficha pos = primeraFicha;
        if(pos == ficha)
            primeraFicha = primeraFicha.sig;
        else{
            while(pos.sig != ficha)
                pos = pos.sig;
            pos.sig = pos.sig.sig;
        }
    }
    
    public Ficha buscarFicha(int id){
        Ficha pos = primeraFicha;
        while (pos != null) {
            if (pos.id == id)
                return pos;
            pos = pos.sig;
        }
        return null;
    }
    
    public void CrearFichas(){
  
    Ficha ficha1 = new Ficha(1,0,0,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/0-0.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/0-0.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/0-0.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/0-0.jpg")).getImage()));
    Ficha ficha2 = new Ficha(2,0,1,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/0-1.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/0-1.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/0-1.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/0-1.jpg")).getImage()));
    Ficha ficha3 = new Ficha(3,0,2,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/0-2.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/0-2.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/0-2.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/0-2.jpg")).getImage()));
    Ficha ficha4 = new Ficha(4,0,3,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/0-3.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/0-3.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/0-3.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/0-3.jpg")).getImage()));
    Ficha ficha5 = new Ficha(5,0,4,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/0-4.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/0-4.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/0-4.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/0-4.jpg")).getImage()));
    Ficha ficha6 = new Ficha(6,0,5,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/0-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/0-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/0-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/0-5.jpg")).getImage()));
    Ficha ficha7 = new Ficha(7,0,6,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/0-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/0-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/0-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/0-6.jpg")).getImage()));
    Ficha ficha8 = new Ficha(8,1,1,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/1-1.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/1-1.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/1-1.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/1-1.jpg")).getImage()));
    Ficha ficha9 = new Ficha(9,1,2,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/1-2.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/1-2.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/1-2.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/1-2.jpg")).getImage()));
    Ficha ficha10 = new Ficha(10,1,3,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/1-3.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/1-3.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/1-3.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/1-3.jpg")).getImage()));
    Ficha ficha11 = new Ficha(11,1,4,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/1-4.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/1-4.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/1-4.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/1-4.jpg")).getImage()));
    Ficha ficha12 = new Ficha(12,1,5,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/1-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/1-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/1-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/1-5.jpg")).getImage()));
    Ficha ficha13 = new Ficha(13,1,6,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/1-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/1-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/1-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/1-6.jpg")).getImage()));
    Ficha ficha14 = new Ficha(14,2,2,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/2-2.jpg")).getImage()),new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/2-2.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/2-2.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/2-2.jpg")).getImage()));
    Ficha ficha15 = new Ficha(15,2,3,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/2-3.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/2-3.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/2-3.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/2-3.jpg")).getImage()));
    Ficha ficha16 = new Ficha(16,2,4,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/2-4.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/2-4.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/2-4.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/2-4.jpg")).getImage()));
    Ficha ficha17 = new Ficha(17,2,5,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/2-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/2-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/2-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/2-5.jpg")).getImage()));
    Ficha ficha18 = new Ficha(18,2,6,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/2-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/2-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/2-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/2-6.jpg")).getImage()));
    Ficha ficha19 = new Ficha(19,3,3,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/3-3.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/3-3.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/3-3.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/3-3.jpg")).getImage()));
    Ficha ficha20 = new Ficha(20,3,4,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/3-4.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/3-4.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/3-4.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/3-4.jpg")).getImage()));
    Ficha ficha21 = new Ficha(21,3,5,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/3-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/3-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/3-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/3-5.jpg")).getImage()));
    Ficha ficha22 = new Ficha(22,3,6,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/3-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/3-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/3-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/3-6.jpg")).getImage()));
    Ficha ficha23 = new Ficha(23,4,4,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/4-4.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/4-4.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/4-4.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/4-4.jpg")).getImage()));
    Ficha ficha24 = new Ficha(24,4,5,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/4-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/4-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/4-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/4-5.jpg")).getImage()));
    Ficha ficha25 = new Ficha(25,4,6,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/4-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/4-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/4-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/4-6.jpg")).getImage()));
    Ficha ficha26 = new Ficha(26,5,5,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/5-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/5-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/5-5.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/5-5.jpg")).getImage()));
    Ficha ficha27 = new Ficha(27,5,6,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/5-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/5-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/5-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/5-6.jpg")).getImage()));
    Ficha ficha28 = new Ficha(28,6,6,0,0,new ImageIcon(new ImageIcon(getClass().getResource("Piezas1/6-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas2/6-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas3/6-6.jpg")).getImage()), new ImageIcon(new ImageIcon(getClass().getResource("Piezas4/6-6.jpg")).getImage()));
    
    agregarFicha(ficha1);
    agregarFicha(ficha2);
    agregarFicha(ficha3);
    agregarFicha(ficha4);
    agregarFicha(ficha5);
    agregarFicha(ficha6);
    agregarFicha(ficha7);
    agregarFicha(ficha8);
    agregarFicha(ficha9);
    agregarFicha(ficha10);
    agregarFicha(ficha11);
    agregarFicha(ficha12);
    agregarFicha(ficha13);
    agregarFicha(ficha14);
    agregarFicha(ficha15);
    agregarFicha(ficha16);
    agregarFicha(ficha17);
    agregarFicha(ficha18);
    agregarFicha(ficha19);
    agregarFicha(ficha20);
    agregarFicha(ficha21);
    agregarFicha(ficha22);
    agregarFicha(ficha23);
    agregarFicha(ficha24);
    agregarFicha(ficha25);
    agregarFicha(ficha26);
    agregarFicha(ficha27);
    agregarFicha(ficha28);
   }
}
