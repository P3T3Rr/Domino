
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;


public class Login extends javax.swing.JFrame {

    JPasswordField password = new JPasswordField();
    Metodos met = new Metodos();
    public Usuario usuario;
    
    public Login() throws FileNotFoundException{
        this.setTitle("Login");
        this.setResizable(false);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/descarga.png")).getImage());
        met.leer();
        initComponents();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jEditorPane1 = new javax.swing.JEditorPane();
        register = new javax.swing.JButton();
        login = new javax.swing.JButton();
        Correo1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        Contrasena = new javax.swing.JPasswordField();
        jLabel5 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        jScrollPane1.setViewportView(jEditorPane1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocation(new java.awt.Point(500, 200));
        setMaximumSize(new java.awt.Dimension(1030, 650));
        setMinimumSize(new java.awt.Dimension(1030, 650));
        setPreferredSize(new java.awt.Dimension(1030, 650));
        getContentPane().setLayout(null);

        register.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        register.setText("REGISTER");
        register.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registerActionPerformed(evt);
            }
        });
        getContentPane().add(register);
        register.setBounds(840, 200, 130, 30);

        login.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        login.setText("LOGIN");
        login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginActionPerformed(evt);
            }
        });
        getContentPane().add(login);
        login.setBounds(650, 200, 140, 30);

        Correo1.setFont(new java.awt.Font("Consolas", 0, 18)); // NOI18N
        Correo1.setText("123");
        Correo1.setToolTipText("correo");
        Correo1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Correo1ActionPerformed(evt);
            }
        });
        getContentPane().add(Correo1);
        Correo1.setBounds(660, 80, 120, 30);

        jLabel2.setFont(new java.awt.Font("Consolas", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Password");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(670, 120, 110, 29);

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setFont(new java.awt.Font("Consolas", 1, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Id");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(710, 50, 30, 30);

        Contrasena.setFont(new java.awt.Font("Consolas", 0, 18)); // NOI18N
        Contrasena.setText("123");
        Contrasena.setToolTipText("contraseña");
        getContentPane().add(Contrasena);
        Contrasena.setBounds(660, 150, 120, 30);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Register-Now-on-caronic_opt (1).png"))); // NOI18N
        getContentPane().add(jLabel5);
        jLabel5.setBounds(840, 70, 130, 120);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Login.png"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 1050, 600);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void loginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loginActionPerformed
        String contrasena = "";
        char []  contra = Contrasena.getPassword();
        for(int i=0; i<contra.length; i++) contrasena += contra[i];
       
        if (Correo1.getText().isEmpty() || "".equals(contrasena) ) 
            JOptionPane.showMessageDialog(null, "Campo Vacio...!!");
        else{
            boolean x = met.InicioDeSesion(Correo1.getText(), contrasena);
            if (x == true){
                usuario = met.buscarUsuario(Correo1.getText());
                if (Correo1.getText().equals("2017102058")){
                    AdmiAgregar frame = new AdmiAgregar(this, met);
                    this.setVisible(false);
                    frame.setVisible(true);
                }
                else{
                    UsuarioNuevoJuego frame = new UsuarioNuevoJuego(this, met);
                    this.setVisible(false);
                    frame.setVisible(true);
                }
            }    
        }
    }//GEN-LAST:event_loginActionPerformed

    private void registerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registerActionPerformed
        Register frame = new Register(this, met);
        frame.setVisible(true);
    }//GEN-LAST:event_registerActionPerformed

    private void Correo1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Correo1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Correo1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new Login().setVisible(true);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField Contrasena;
    private javax.swing.JTextField Correo1;
    private javax.swing.JEditorPane jEditorPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton login;
    private javax.swing.JButton register;
    // End of variables declaration//GEN-END:variables
}
