
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;


public class AdmiVer extends javax.swing.JFrame {
    Login login;
    Metodos met;
    
    DefaultListModel<String>ListModel2 = new DefaultListModel<>();
    public Usuario pos;
    
    public AdmiVer(Login login,  Metodos met) {
        this.login = login;
        this.met = met;
        this.setTitle("Administrador");
        this.setResizable(false);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/descarga.png")).getImage());
        initComponents();
        if (met.inicio == null){
            ListModel2.clear();
            ListModel2.addElement("Lista Vacia");
            jList2.setModel(ListModel2);
        }
        else{
            pos = met.inicio;
            Imprimir2();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Editar = new javax.swing.JButton();
        Ver = new javax.swing.JButton();
        Agregar = new javax.swing.JButton();
        Eliminar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        atras = new javax.swing.JButton();
        ant = new javax.swing.JButton();
        sig = new javax.swing.JButton();
        fin = new javax.swing.JButton();
        inicio = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocation(new java.awt.Point(500, 200));
        setMaximumSize(new java.awt.Dimension(900, 590));
        setMinimumSize(new java.awt.Dimension(900, 590));
        setPreferredSize(new java.awt.Dimension(900, 590));
        getContentPane().setLayout(null);

        Editar.setBackground(new java.awt.Color(204, 204, 204));
        Editar.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        Editar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/editar usuario_opt.png"))); // NOI18N
        Editar.setText("Editar Usuario");
        Editar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Editar.setBorderPainted(false);
        Editar.setIconTextGap(15);
        Editar.setOpaque(false);
        Editar.setSelected(true);
        Editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EditarActionPerformed(evt);
            }
        });
        getContentPane().add(Editar);
        Editar.setBounds(-20, 250, 290, 60);

        Ver.setBackground(new java.awt.Color(102, 102, 102));
        Ver.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        Ver.setForeground(new java.awt.Color(243, 243, 243));
        Ver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ver usuarios_opt.png"))); // NOI18N
        Ver.setText("Ver Usuarios");
        Ver.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Ver.setBorderPainted(false);
        Ver.setIconTextGap(20);
        Ver.setOpaque(false);
        Ver.setSelected(true);
        Ver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VerActionPerformed(evt);
            }
        });
        getContentPane().add(Ver);
        Ver.setBounds(-10, 440, 280, 80);

        Agregar.setBackground(new java.awt.Color(204, 204, 204));
        Agregar.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        Agregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/agregar usuario_opt.png"))); // NOI18N
        Agregar.setText("Agregar Usuario");
        Agregar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Agregar.setBorderPainted(false);
        Agregar.setIconTextGap(10);
        Agregar.setOpaque(false);
        Agregar.setSelected(true);
        Agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarActionPerformed(evt);
            }
        });
        getContentPane().add(Agregar);
        Agregar.setBounds(-10, 180, 280, 60);

        Eliminar.setBackground(new java.awt.Color(204, 204, 204));
        Eliminar.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        Eliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/elinimar usuario_opt.png"))); // NOI18N
        Eliminar.setText("Eliminar Usuario");
        Eliminar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Eliminar.setBorderPainted(false);
        Eliminar.setIconTextGap(10);
        Eliminar.setOpaque(false);
        Eliminar.setSelected(true);
        Eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EliminarActionPerformed(evt);
            }
        });
        getContentPane().add(Eliminar);
        Eliminar.setBounds(-10, 320, 280, 60);

        jPanel1.setBackground(new java.awt.Color(119, 119, 119));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(null);

        jLabel2.setBackground(new java.awt.Color(76, 76, 76));
        jLabel2.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(221, 221, 221));
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ver usuarios_opt.png"))); // NOI18N
        jLabel2.setText("Ver Usuarios");
        jLabel2.setIconTextGap(30);
        jPanel1.add(jLabel2);
        jLabel2.setBounds(100, 90, 380, 62);

        atras.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        atras.setToolTipText("Salir");
        atras.setBorder(null);
        atras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                atrasActionPerformed(evt);
            }
        });
        jPanel1.add(atras);
        atras.setBounds(523, 8, 68, 0);

        ant.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        ant.setText("<<");
        ant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                antActionPerformed(evt);
            }
        });
        jPanel1.add(ant);
        ant.setBounds(90, 420, 72, 63);

        sig.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        sig.setText(">>");
        sig.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sigActionPerformed(evt);
            }
        });
        jPanel1.add(sig);
        sig.setBounds(440, 420, 72, 63);

        fin.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        fin.setText("Ultimo");
        fin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finActionPerformed(evt);
            }
        });
        jPanel1.add(fin);
        fin.setBounds(300, 420, 141, 63);

        inicio.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        inicio.setText("Inicio");
        inicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inicioActionPerformed(evt);
            }
        });
        jPanel1.add(inicio);
        inicio.setBounds(160, 420, 140, 63);

        jList2.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
        jScrollPane2.setViewportView(jList2);

        jPanel1.add(jScrollPane2);
        jScrollPane2.setBounds(90, 200, 423, 217);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(280, 0, 610, 550);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/foto de perfil_opt (1).png"))); // NOI18N
        getContentPane().add(jLabel3);
        jLabel3.setBounds(30, 70, 60, 60);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/foto de portada_opt (2).png"))); // NOI18N
        getContentPane().add(jLabel4);
        jLabel4.setBounds(-20, 0, 300, 100);

        jLabel5.setBackground(new java.awt.Color(0, 0, 0));
        jLabel5.setFont(new java.awt.Font("Dialog", 1, 20)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Jeremmy Soto");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(130, 100, 150, 30);

        jLabel1.setBackground(new java.awt.Color(204, 204, 204));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fonde_Juego_opt.jpg"))); // NOI18N
        jLabel1.setText("Jeremmy");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 280, 550);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void EditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EditarActionPerformed
        AdmiEditar frame = new AdmiEditar(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_EditarActionPerformed

    private void AgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarActionPerformed
        AdmiAgregar frame = new AdmiAgregar(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_AgregarActionPerformed

    private void atrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_atrasActionPerformed
        login.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_atrasActionPerformed

    private void EliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EliminarActionPerformed
        AdmiEliminar frame = new AdmiEliminar(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_EliminarActionPerformed

    private void antActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_antActionPerformed
        if (met.inicio == null){
            ListModel2.clear();
            ListModel2.addElement("Lista Vacia");
            jList2.setModel(ListModel2);
        }

        else  pos = pos.ant;

        Imprimir2();
    }//GEN-LAST:event_antActionPerformed

    private void sigActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sigActionPerformed
        if (met.inicio == null){
            ListModel2.clear();
            ListModel2.addElement("Lista Vacia");
            jList2.setModel(ListModel2);
        }

        else pos = pos.sig;

        Imprimir2();
    }//GEN-LAST:event_sigActionPerformed

    private void finActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finActionPerformed
        if (met.inicio == null){
            ListModel2.clear();
            ListModel2.addElement("Lista Vacia");
            jList2.setModel(ListModel2);
        }
        else{
            pos = met.fin;
            Imprimir2();
        }
    }//GEN-LAST:event_finActionPerformed

    private void inicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inicioActionPerformed
        if (met.inicio == null){
            ListModel2.clear();
            ListModel2.addElement("Lista Vacia");
            jList2.setModel(ListModel2);
        }
        else{
            pos = met.inicio;
            Imprimir2();
        }
    }//GEN-LAST:event_inicioActionPerformed

    private void VerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VerActionPerformed
        AdmiVer frame = new AdmiVer(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_VerActionPerformed

    public void Imprimir2 (){
        
        ListModel2.clear();
        
        if(met.inicio == null) ListModel2.addElement("Lista Vacia");
        
        else{
            ListModel2.addElement(" "+pos.nombre);
            ListModel2.addElement(" "+pos.cedula);
            jList2.setModel(ListModel2);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Agregar;
    private javax.swing.JButton Editar;
    private javax.swing.JButton Eliminar;
    private javax.swing.JButton Ver;
    private javax.swing.JButton ant;
    private javax.swing.JButton atras;
    private javax.swing.JButton fin;
    private javax.swing.JButton inicio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JList<String> jList2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton sig;
    // End of variables declaration//GEN-END:variables
}
