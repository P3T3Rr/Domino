
import java.awt.AWTException;
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Juego extends Canvas implements MouseListener, KeyListener {  
    Metodos met;
    Login login;
    UsuarioNuevoJuego NJ;
    
    Ficha seleccionada;
    
    ArrayList<Ficha> misFichas = new ArrayList<>();
    ArrayList<Ficha> fichasOponente = new ArrayList<>();
    
    Ficha Inicio;
    Ficha Fin;
    
    Calendar calendario = Calendar.getInstance();
    int hora =calendario.get(Calendar.HOUR_OF_DAY);
    int minuto = calendario.get(Calendar.MINUTE);
    int segundo = calendario.get(Calendar.SECOND);
    
    boolean JuegoYo;
    boolean trampa1 = true;
    boolean trampa2 = true;

    public int Vuelta = 1;
    
    public boolean PierdeTurno = false;

    public Juego(Metodos met, Login login) {
        this.login = login;
        this.met = met;
        addMouseListener(this);
        addKeyListener(this);
        agregarPrimerasFichas();
        JuegoYo = quienVaPrimero();
    }
    
    public void paint(Graphics g) {
        super.paint(g);
        
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/fonde_Juego_opt - copia.jpg")).getImage(), 30, 170, 1630, 650, null);
        
        if (fichasOponente.isEmpty()){
            JOptionPane.showMessageDialog(null, "YOU LOSE...!!");
            g.dispose();
        }
        if (misFichas.isEmpty())
            JOptionPane.showMessageDialog(null, "YOU WIN...!!");
        
        if (JuegoYo){
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Su turno.png")).getImage(), 1500, 870, null);
            if (trampa1){
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Retire ficha.png")).getImage(), 1700, 50, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Un turno.png")).getImage(), 1700, 90, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Ficha nueva.png")).getImage(), 1700, 130, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Trampas.png")).getImage(), 1690, 3, null);
            }
        }
        if (!JuegoYo){
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Su turno.png")).getImage(), 1500, 50, null);
            if (trampa2){
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Retire ficha.png")).getImage(), 1700, 50, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Un turno.png")).getImage(), 1700, 90, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Ficha nueva.png")).getImage(), 1700, 130, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Trampas.png")).getImage(), 1690, 3, null);
            }
        }
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Preciona Click Derecho.png")).getImage(), 30, 830, null);
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/TomarFicha.png")).getImage(), 1680, 200, null);
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/pasar_opt.png")).getImage(), 1715, 830, null);
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/guardar_opt.png")).getImage(), 1580, 180, null);
        
        int x = 30;
        for (int i=0; i<misFichas.size(); i++){
            misFichas.get(i).x = x;
            misFichas.get(i).y = 865;
            x += 75;
            g.drawImage(misFichas.get(i).imagen1.getImage(), misFichas.get(i).x, misFichas.get(i).y, 72, 144, null);
        }
        
        x = 30;
        for (int i=0; i<fichasOponente.size(); i++){
            fichasOponente.get(i).x = x;
            fichasOponente.get(i).y = 10;
            x += 75;
            g.drawImage(fichasOponente.get(i).imagen1.getImage(), fichasOponente.get(i).x, fichasOponente.get(i).y, 72, 144, null);
        }
        
        imprimirJuego(g);
        
        if(seleccionada!=null){
            if (Vuelta == 1 || Vuelta == 3){
                seleccionada.ancho = 72;
                seleccionada.largo = 144;
            }
            else{
                seleccionada.ancho = 144;
                seleccionada.largo = 72;
            }
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Preciona Espacio.png")).getImage(), 1680, 500, null);
            imprimirSeleccionada(g);
        }
    }
    
    @Override
    public void mouseClicked(MouseEvent e) { 
        //Cuando preciona click izquierdo
        if (e.getButton() == MouseEvent.BUTTON1) { 
            // si preciona el boton de ficha nueva
            if ((e.getX() >= 1720) && (e.getX() <= 1820) && (e.getY() >= 290) && (e.getY() <= 390)) {
                agregarFicha();
                repaint();
            }
            // si preciona el boton de pasar de turno
            else if ((e.getX() >= 1720) && (e.getX() <= 1820) && (e.getY() >= 870) && (e.getY() <= 960)){
                JuegoYo = !JuegoYo;
                repaint();
            }
            // si preciona el boton de guardar
            else if((e.getX() >= 1580) && (e.getX() <= 1650) && (e.getY() >= 180) && (e.getY() <= 250)){
                JOptionPane.showMessageDialog(null, "Partida Guardada...!!");
                try {
                    guardarPartida();
                } catch (IOException ex) {
                    Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
                } catch (AWTException ex) {
                    Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            // si preciona preciona el boton de trampa 1
            else if ((e.getX() >= 1700) && (e.getX() <= 1832) && (e.getY() >= 50) && (e.getY() <= 72)){
                if (seleccionada != null){
                    if (JuegoYo){
                        seleccionada.trampa = 1;
                        trampa1 = false;
                    }
                    else{
                        seleccionada.trampa = 1;
                        trampa2 = false;
                    }
                    repaint();
                }
            }
            // si preciona preciona el boton de trampa 2
            else if ((e.getX() >= 1700) && (e.getX() <= 1832) && (e.getY() >= 90) && (e.getY() <= 112)){
                if (seleccionada != null){
                    if (JuegoYo){
                        seleccionada.trampa = 2;
                        trampa1 = false;
                    }
                    else{
                        seleccionada.trampa = 2;
                        trampa2 = false;
                    }
                    repaint();
                }
            }
            // si preciona preciona el boton de trampa 3
            else if ((e.getX() >= 1700) && (e.getX() <= 1832) && (e.getY() >= 130) && (e.getY() <= 152)){
                if (seleccionada != null){
                    if (JuegoYo){
                        seleccionada.trampa = 3;
                        trampa1 = false;
                    }
                    else{
                        seleccionada.trampa = 3;
                        trampa2 = false;
                    }
                    repaint();
                }
            }
            //si preciona cualquier otro lugar
            else{
                //si juego yo, recorra mi lista de fichas y vea si preciono ensima de una de ellas
                if (JuegoYo)
                    for(int i=0; i<misFichas.size(); i++){
                        if ((e.getX() >= misFichas.get(i).x) && (e.getX() <= misFichas.get(i).x + 72) && (e.getY() >= misFichas.get(i).y) && (e.getY() <= misFichas.get(i).y + 144)) {
                            seleccionada = misFichas.get(i);
                            repaint();
                            return;
                        }
                    }
                //si no juego yo, recorra la lista de fichas de mi oponente y vea si preciono ensima de una de ellas
                else
                    for(int i=0; i<fichasOponente.size(); i++){
                        if ((e.getX() >= fichasOponente.get(i).x) && (e.getX() <= fichasOponente.get(i).x + 72) && (e.getY() >= fichasOponente.get(i).y) && (e.getY() <= fichasOponente.get(i).y + 144)) {
                            seleccionada = fichasOponente.get(i);
                            repaint();
                            return;
                        }
                    }
                //si no, coloque la ficha seleccionada
                if (seleccionada != null){
                    agregar(e.getX(), e.getY());
                }    
            }
        }
    }
    
    @Override
    public void keyPressed(KeyEvent e) { //Tecla de Espacio
        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            if (Vuelta == 4)
                Vuelta = 0;
            Vuelta += 1;
            repaint();
        }
    } //Girar laimagen
    
    public void imprimirSeleccionada(Graphics g){ 
        if(Vuelta==1)
            g.drawImage(seleccionada.imagen1.getImage(), 1740, 590, seleccionada.ancho, seleccionada.largo, null);
        if(Vuelta==2)
            g.drawImage(seleccionada.imagen2.getImage(), 1704, 626, seleccionada.ancho, seleccionada.largo, null);
        if(Vuelta==3)
            g.drawImage(seleccionada.imagen3.getImage(), 1740, 590, seleccionada.ancho, seleccionada.largo, null);
        if(Vuelta==4)
            g.drawImage(seleccionada.imagen4.getImage(), 1704, 626, seleccionada.ancho, seleccionada.largo, null);
    } //Imprime la ficha seleccionada en una posicion especifica
    
    public void imprimirJuego (Graphics g){ 
        //Imprime la primera ficha en juego
        if(Inicio.imagen==1)
            g.drawImage(Inicio.imagen1.getImage(), Inicio.x, Inicio.y, Inicio.ancho, Inicio.largo, null);
        if(Inicio.imagen==2)
           g.drawImage(Inicio.imagen2.getImage(), Inicio.x, Inicio.y, Inicio.ancho, Inicio.largo, null);
        if(Inicio.imagen==3)
           g.drawImage(Inicio.imagen3.getImage(), Inicio.x, Inicio.y, Inicio.ancho, Inicio.largo, null);
        if(Inicio.imagen==4)
           g.drawImage(Inicio.imagen4.getImage(), Inicio.x, Inicio.y, Inicio.ancho, Inicio.largo, null);
        
        Ficha pos = Inicio.enlace2;
        
        //Si hay mas de una ficha en el juego
        if (Inicio != Fin)
            while(pos != null){
                if(pos.imagen==1)
                    g.drawImage(pos.imagen1.getImage(), pos.x, pos.y, pos.ancho, pos.largo, null);
                if(pos.imagen==2)
                   g.drawImage(pos.imagen2.getImage(), pos.x, pos.y, pos.ancho, pos.largo, null);
                if(pos.imagen==3)
                   g.drawImage(pos.imagen3.getImage(), pos.x, pos.y, pos.ancho, pos.largo, null);
                if(pos.imagen==4)
                   g.drawImage(pos.imagen4.getImage(), pos.x, pos.y, pos.ancho, pos.largo, null);
                
                if(pos.enlace3!=null){
                    Ficha pos2 = pos.enlace3;
                    while(pos2 != null){
                        if(pos2.imagen==1)
                           g.drawImage(pos2.imagen1.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==2)
                           g.drawImage(pos2.imagen2.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==3)
                           g.drawImage(pos2.imagen3.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==4)
                           g.drawImage(pos2.imagen4.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        
                        pos2 = pos2.enlace2;
                    }
                }
                
                if(pos.enlace4!=null){
                    Ficha pos2 = pos.enlace4;
                    while(pos2 != null){
                        if(pos2.imagen==1)
                           g.drawImage(pos2.imagen1.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==2)
                           g.drawImage(pos2.imagen2.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==3)
                           g.drawImage(pos2.imagen3.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==4)
                           g.drawImage(pos2.imagen4.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        
                        pos2 = pos2.enlace2;
                    }
                }

                pos = pos.enlace2;
            }
    } //Imprime las fichas en juego
    
    public void agregarPrimerasFichas(){
        // Agregar mis 7 fichas
        for(int m=0; m<7; m++){
            int random = (int) (Math.random()*28+1);
            Ficha ficha = met.buscarFicha(random);
            while(ficha == null){
                random = (int) (Math.random()*28+1);
                ficha = met.buscarFicha(random);
            }
            misFichas.add(ficha);
            met.eliminarFicha(ficha);
        }
        // Agregar las 7 fichas de mi oponente
        for(int m=0; m<7; m++){
            int random = (int) (Math.random()*28+1);
            Ficha ficha = met.buscarFicha(random);
            while(ficha == null){
                random = (int) (Math.random()*28+1);
                ficha = met.buscarFicha(random);
            }
            fichasOponente.add(ficha);
            met.eliminarFicha(ficha);
        }
    } //Agregar las primeras 14 fichas
    
    public void agregarFicha(){
        if(met.primeraFicha != null){
            int random = (int) (Math.random()*28+1);
            Ficha ficha = met.buscarFicha(random);
            while(ficha == null){
                random = (int) (Math.random()*28+1);
                ficha = met.buscarFicha(random);
            }

            if (JuegoYo){
                misFichas.add(ficha);
                met.eliminarFicha(ficha);
            }
            else{
                fichasOponente.add(ficha);
                met.eliminarFicha(ficha);
            }
            JuegoYo = !JuegoYo;
        }
        else
            JOptionPane.showMessageDialog(null, "Ya no hay más fichas...!!");
    } //Agregar una ficha mas
    
    public boolean quienVaPrimero(){
        int mayor = 6;
        while(true){
            for (int i=0; i<misFichas.size(); i++){
                if (misFichas.get(i).lado1 == mayor && misFichas.get(i).lado2 == mayor){
                    misFichas.get(i).x = 800;
                    misFichas.get(i).y = 450;
                    misFichas.get(i).imagen = 2;
                    misFichas.get(i).ancho = 144;
                    misFichas.get(i).largo = 72;
                    
                    Inicio = Fin = misFichas.get(i);
                    
                    misFichas.remove(misFichas.get(i));
                    return false;
                }
            }

            for (int i=0; i<fichasOponente.size(); i++){
                if (fichasOponente.get(i).lado1 == mayor && fichasOponente.get(i).lado2 == mayor){
                    fichasOponente.get(i).x = 800;
                    fichasOponente.get(i).y = 450;
                    fichasOponente.get(i).imagen = 2;
                    fichasOponente.get(i).ancho = 144;
                    fichasOponente.get(i).largo = 72;
                    
                    Inicio = Fin = fichasOponente.get(i);
                    
                    fichasOponente.remove(fichasOponente.get(i));
                    return true;
                }
            }
            mayor -= 1;
        }
    } //Decide quien pone la primera ficha
    
    public void agregar (int x, int y){
        if (seleccionada.lado1 != seleccionada.lado2){
            Ficha pos = Inicio;
            while (pos != null){ 
                if ((pos.lado1 == seleccionada.lado1) || (pos.lado1 == seleccionada.lado2) || (pos.lado2 == seleccionada.lado2) || (pos.lado2 == seleccionada.lado1)){
                    if (Inicio == Fin){
                        if ((x >= pos.x + 144) && (x <= pos.x + 144 + 72) && (y >= pos.y) && (y <= pos.y + 72)){
                            seleccionada.x = pos.x + 144;
                            seleccionada.y = pos.y;
                            SePuedeConectar(pos); return;
                            
                        }

                        if ((x >= pos.x ) && (x <= pos.x + 144) && (y >= pos.y - 72) && (y <= pos.y )){
                            if (seleccionada.largo != 72){
                                seleccionada.x = pos.x + 36;
                                seleccionada.y = pos.y - seleccionada.largo;
                                SePuedeConectar(pos); return;
                            }
                        }

                        if ((x >= pos.x ) && (x <= pos.x + 144) && (y >= pos.y + 72) && (y <= pos.y + 144 )){
                            if (seleccionada.largo != 72){
                                seleccionada.x = pos.x + 36;
                                seleccionada.y = pos.y + 72;
                                SePuedeConectar(pos); return;
                            }
                        }

                        if ((x >= pos.x - 72) && (x <= pos.x ) && (y >= pos.y ) && (y <= pos.y + 72 )){
                            seleccionada.x = pos.x - seleccionada.ancho;
                            seleccionada.y = pos.y;
                            SePuedeConectar(pos); return;
                        }
                    }
                    else{
                        if (pos != Fin && pos != Inicio && pos.lado1 != pos.lado2)
                            pos = pos.enlace2;
                        if (pos.largo == 72){
                            if (pos.lado1 == pos.lado2){
                                if ((x >= pos.x ) && (x <= pos.x + 144) && (y >= pos.y - 72) && (y <= pos.y )){
                                    if (seleccionada.largo != 72){
                                        seleccionada.x = pos.x + 36;
                                        seleccionada.y = pos.y - seleccionada.largo;
                                        SePuedeConectar(pos); return;
                                    }
                                }

                                if ((x >= pos.x ) && (x <= pos.x + 144) && (y >= pos.y + 72) && (y <= pos.y + 144 )){
                                    if (seleccionada.largo != 72){
                                        seleccionada.x = pos.x + 36;
                                        seleccionada.y = pos.y + 72;
                                        SePuedeConectar(pos); return;
                                    }
                                }
                            }
                            
                            if ((x >= pos.x + 144) && (x <= pos.x + 144 + 72) && (y >= pos.y) && (y <= pos.y + 72)){
                                seleccionada.x = pos.x + 144;
                                seleccionada.y = pos.y;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x + 72) && (x <= pos.x + 144) && (y >= pos.y + 72) && (y <= pos.y + 144)){
                                seleccionada.x = pos.x + 72;
                                seleccionada.y = pos.y + 72;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x + 72) && (x <= pos.x + 144) && (y >= pos.y - 72) && (y <= pos.y )){
                                seleccionada.x = pos.x + 72;
                                seleccionada.y = pos.y - seleccionada.largo;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x ) && (x <= pos.x + 72) && (y >= pos.y - 72) && (y <= pos.y )){
                                seleccionada.x = pos.x;
                                seleccionada.y = pos.y - seleccionada.largo;
                                SePuedeConectar(pos); return;
                            } 
                            if ((x >= pos.x ) && (x <= pos.x + 72) && (y >= pos.y + 72) && (y <= pos.y + 144 )){
                                seleccionada.x = pos.x;
                                seleccionada.y = pos.y + 72;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x - 72) && (x <= pos.x ) && (y >= pos.y ) && (y <= pos.y + 72 )){
                                seleccionada.x = pos.x - seleccionada.ancho;
                                seleccionada.y = pos.y;
                                SePuedeConectar(pos); return;
                            }
                        }
                        else{
                            if (pos.lado1 == pos.lado2){
                                if ((x >= pos.x - 72) && (x <= pos.x ) && (y >= pos.y ) && (y <= pos.y + 144 )){
                                    if (seleccionada.largo != 144){
                                        seleccionada.x = pos.x - seleccionada.ancho;
                                        seleccionada.y = pos.y + 36;
                                        SePuedeConectar(pos); return;
                                    }
                                }
                                if ((x >= pos.x + 72) && (x <= pos.x + 144) && (y >= pos.y ) && (y <= pos.y + 144 )){
                                    if (seleccionada.largo != 144){
                                        seleccionada.x = pos.x + 72;
                                        seleccionada.y = pos.y + 36;
                                        SePuedeConectar(pos); return;
                                    }
                                }
                            }
                            
                            if ((x >= pos.x + 72) && (x <= pos.x + 144 ) && (y >= pos.y) && (y <= pos.y + 72)){
                                seleccionada.x = pos.x + 72;
                                seleccionada.y = pos.y;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x + 72) && (x <= pos.x + 144) && (y >= pos.y + 72) && (y <= pos.y + 144)){
                                seleccionada.x = pos.x + 72;
                                seleccionada.y = pos.y + 72;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x ) && (x <= pos.x + 72) && (y >= pos.y + 144) && (y <= pos.y + 144 + 72)){
                                seleccionada.x = pos.x;
                                seleccionada.y = pos.y + 144;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x - 72) && (x <= pos.x ) && (y >= pos.y + 72) && (y <= pos.y + 144)){
                                seleccionada.x = pos.x - seleccionada.ancho;
                                seleccionada.y = pos.y + 72;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x - 72) && (x <= pos.x ) && (y >= pos.y ) && (y <= pos.y + 72)){
                                seleccionada.x = pos.x - seleccionada.ancho;
                                seleccionada.y = pos.y;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x ) && (x <= pos.x + 72 ) && (y >= pos.y - 72) && (y <= pos.y )){
                                seleccionada.x = pos.x;
                                seleccionada.y = pos.y - seleccionada.largo;
                                SePuedeConectar(pos); return;
                            }
                        }
                    }
                }
                pos = pos.enlace2;
            }  
        }
        else{
            Ficha pos = Inicio;
            while (pos != null){
                if (pos.lado1!=pos.lado2 && pos!=Inicio && pos!= Fin)
                    pos = pos.enlace2;
                if ((pos.lado1 == seleccionada.lado1) || (pos.lado1 == seleccionada.lado2) || (pos.lado2 == seleccionada.lado2) || (pos.lado2 == seleccionada.lado1)){
                    if (pos.largo == 72){
                        if ((x >= pos.x + 144) && (x <= pos.x + 144 + 72) && (y >= pos.y) && (y <= pos.y + 72)){
                            if (seleccionada.largo == 72){
                                seleccionada.x = pos.x + 144;
                                seleccionada.y = pos.y; return;
                            }
                            else{
                                seleccionada.x = pos.x + 144;
                                seleccionada.y = pos.y - 36; 
                            }
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x - 72) && (x <= pos.x ) && (y >= pos.y ) && (y <= pos.y + 72 )){
                            if (seleccionada.largo == 72){
                                seleccionada.x = pos.x - seleccionada.ancho;
                                seleccionada.y = pos.y;
                            }
                            else{
                                seleccionada.x = pos.x - seleccionada.ancho;
                                seleccionada.y = pos.y - 36;
                            }
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x + 72) && (x <= pos.x + 144) && (y >= pos.y + 72) && (y <= pos.y + 144)){
                            seleccionada.x = pos.x + 72;
                            seleccionada.y = pos.y + 72;
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x + 72) && (x <= pos.x + 144) && (y >= pos.y - 72) && (y <= pos.y )){
                            seleccionada.x = pos.x + 72;
                            seleccionada.y = pos.y - seleccionada.largo;
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x ) && (x <= pos.x + 72) && (y >= pos.y - 72) && (y <= pos.y )){
                            seleccionada.x = pos.x;
                            seleccionada.y = pos.y - seleccionada.largo;
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x ) && (x <= pos.x + 72) && (y >= pos.y + 72) && (y <= pos.y + 144 )){
                            seleccionada.x = pos.x;
                            seleccionada.y = pos.y + 72;
                            SePuedeConectar(pos); return;
                        }
                    }
                    else{
                        if ((x >= pos.x ) && (x <= pos.x + 72) && (y >= pos.y + 144) && (y <= pos.y + 144 + 72)){
                            if (seleccionada.largo == 144){
                                seleccionada.x = pos.x;
                                seleccionada.y = pos.y + 144;
                            }
                            else{
                                seleccionada.x = pos.x-36;
                                seleccionada.y = pos.y + 144;
                            }
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x ) && (x <= pos.x + 72 ) && (y >= pos.y - 72) && (y <= pos.y )){
                            if (seleccionada.largo == 144){
                                seleccionada.x = pos.x;
                                seleccionada.y = pos.y - seleccionada.largo;
                            }
                            else{
                                seleccionada.x = pos.x-36;
                                seleccionada.y = pos.y - seleccionada.largo;
                            }
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x + 72) && (x <= pos.x + 144 ) && (y >= pos.y) && (y <= pos.y + 72)){
                            seleccionada.x = pos.x + 72;
                            seleccionada.y = pos.y;
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x + 72) && (x <= pos.x + 144) && (y >= pos.y + 72) && (y <= pos.y + 144)){
                            seleccionada.x = pos.x + 72;
                            seleccionada.y = pos.y + 72;
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x - 72) && (x <= pos.x ) && (y >= pos.y + 72) && (y <= pos.y + 144)){
                            seleccionada.x = pos.x - seleccionada.ancho;
                            seleccionada.y = pos.y + 72;
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x - 72) && (x <= pos.x ) && (y >= pos.y ) && (y <= pos.y + 72)){
                            seleccionada.x = pos.x - seleccionada.ancho;
                            seleccionada.y = pos.y;
                            SePuedeConectar(pos); return;
                        }
                    }
                }
                pos = pos.enlace2;
            }
        }
        repaint();
    } //Se pregunta donde quiere colocar la ficha
    
    public void SePuedeConectar (Ficha ficha){
        seleccionada.imagen = Vuelta;
        
        if (ficha.lado1 == ficha.lado2){
            if (ficha == Inicio){
                seleccionada.enlace2 = Inicio;
                Inicio.enlace1 = seleccionada;
                Inicio = seleccionada;
                if(JuegoYo){
                    misFichas.remove(seleccionada);
                    if (!PierdeTurno)
                        JuegoYo = false;
                    else
                        PierdeTurno = false;
                }
                else{
                    fichasOponente.remove(seleccionada);
                    if (!PierdeTurno)
                        JuegoYo = true;
                    else
                        PierdeTurno = false;
                }
                seleccionada = null;
                repaint();
            }

            else if (ficha == Fin){
                seleccionada.enlace1 = Fin;
                Fin.enlace2 = seleccionada;
                Fin = seleccionada;
                if(JuegoYo){
                    misFichas.remove(seleccionada);
                    if (!PierdeTurno)
                        JuegoYo = false;
                    else
                        PierdeTurno = false;
                }
                else{
                    fichasOponente.remove(seleccionada);
                    if (!PierdeTurno)
                        JuegoYo = true;
                    else
                        PierdeTurno = false;
                }
                seleccionada = null;
                repaint();
            }
            else if(ficha.enlace1 != null && ficha.enlace2 != null && ficha.enlace3 == null){
                seleccionada.enlace1 = ficha;
                ficha.enlace3 = seleccionada;
                if(JuegoYo){
                    misFichas.remove(seleccionada);
                    if (!PierdeTurno)
                        JuegoYo = false;
                    else
                        PierdeTurno = false;
                }
                else{
                    fichasOponente.remove(seleccionada);
                    if (!PierdeTurno)
                        JuegoYo = true;
                    else
                        PierdeTurno = false;
                }
                seleccionada = null;
            }
            else if(ficha.enlace1 != null && ficha.enlace2 != null && ficha.enlace3 != null && ficha.enlace4 == null){
                seleccionada.enlace1 = ficha;
                ficha.enlace4 = seleccionada;
                if(JuegoYo){
                    misFichas.remove(seleccionada);
                    if (!PierdeTurno)
                        JuegoYo = false;
                    else
                        PierdeTurno = false;
                }
                else{
                    fichasOponente.remove(seleccionada);
                    if (!PierdeTurno)
                        JuegoYo = true;
                    else
                        PierdeTurno = false;
                }
                seleccionada = null;
            }
        }
        else{
            if (ficha == Inicio){
                seleccionada.enlace2 = Inicio;
                Inicio.enlace1 = seleccionada;
                Inicio = seleccionada;
                if(JuegoYo){
                    misFichas.remove(seleccionada);
                    if (!PierdeTurno)
                        JuegoYo = false;
                    else
                        PierdeTurno = false;
                }
                else{
                    fichasOponente.remove(seleccionada);
                    if (!PierdeTurno)
                        JuegoYo = true;
                    else
                        PierdeTurno = false;
                }
                seleccionada = null;
                repaint();
            }

            else if (ficha == Fin){
                seleccionada.enlace1 = Fin;
                Fin.enlace2 = seleccionada;
                Fin = seleccionada;
                if(JuegoYo){
                    misFichas.remove(seleccionada);
                    if (!PierdeTurno)
                        JuegoYo = false;
                    else
                        PierdeTurno = false;
                }
                else{
                    fichasOponente.remove(seleccionada);
                    if (!PierdeTurno)
                        JuegoYo = true;
                    else
                        PierdeTurno = false;
                }
                seleccionada = null;
                repaint();
            }
        }
        
        if (ficha.enlace1 != null && ficha.enlace2 != null && Inicio != Fin){

            if (ficha.trampa == 3){ // trampa de agregar ficha
                JOptionPane.showMessageDialog(null, "Trampa activada...!! Se le agregará otra ficha");
                JuegoYo = !JuegoYo;
                agregarFicha();
            }
            if (ficha.trampa == 2){ //trampa de pierde turno
                PierdeTurno = true;
                JOptionPane.showMessageDialog(null, "Trampa activada...!! A perdido un turno");
            }
            if(ficha.trampa == 1){
                JOptionPane.showMessageDialog(null, "Trampa activada...!! se le a retirado la ficha");
            }
        }
        repaint();
    } //Se pregunta en que extremo de la lista se va a colocar la ficha

    public void guardarPartida() throws IOException, AWTException{

        int juegoYo;
        if (JuegoYo)
            juegoYo = 1;
        else
            juegoYo = 2;
       
        Partida nueva = new Partida( seleccionada, misFichas, fichasOponente, Inicio, Fin, juegoYo, trampa1, trampa2, Vuelta);
        this.login.usuario.setPartida(nueva);
        met.guardar();
    }
    
    @Override
    public void mousePressed(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}