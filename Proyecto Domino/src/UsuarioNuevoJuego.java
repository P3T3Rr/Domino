
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;


public final class UsuarioNuevoJuego extends javax.swing.JFrame {

    Login login;
    Metodos met;
    ArrayList<Usuario> listaOponentes = new ArrayList<>();
    ArrayList<Usuario> OponentesSeleccionados = new ArrayList<>();
    int oponenteSeleccionado = 0;
    
    public UsuarioNuevoJuego(Login login,  Metodos met) {
        this.login = login;
        this.met = met;
        this.setTitle("Nuevo Juego");
        this.setResizable(false);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/descarga.png")).getImage());
        initComponents();
        usuario.setText(String.valueOf(login.usuario.nombre));
        InsertarOponentesEnLista();
        SeleccionarOponentes.setText(listaOponentes.get(oponenteSeleccionado).nombre);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        usuario = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        NuevoJuego = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        salir = new javax.swing.JButton();
        atras3 = new javax.swing.JButton();
        configurar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        Personas = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        Oponentes = new javax.swing.JTextArea();
        seleccionarOponente = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        Oponente = new javax.swing.JLabel();
        SeleccionarOponentes = new javax.swing.JTextField();
        siguiente = new javax.swing.JButton();
        anterior = new javax.swing.JButton();
        agregarOponente = new javax.swing.JButton();
        Jugar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setLocation(new java.awt.Point(400, 200));
        setMaximumSize(new java.awt.Dimension(1200, 700));
        setMinimumSize(new java.awt.Dimension(1200, 700));
        setPreferredSize(new java.awt.Dimension(1200, 700));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(null);

        jLabel3.setFont(new java.awt.Font("Magneto", 1, 48)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Main");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(20, 10, 140, 50);

        usuario.setFont(new java.awt.Font("Dialog", 3, 24)); // NOI18N
        usuario.setForeground(new java.awt.Color(255, 255, 255));
        usuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        usuario.setText("Nombre");
        usuario.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(usuario);
        usuario.setBounds(290, 10, 110, 40);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));
        jPanel1.setLayout(null);

        NuevoJuego.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        NuevoJuego.setText("Nuevo Juego");
        NuevoJuego.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        NuevoJuego.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NuevoJuegoActionPerformed(evt);
            }
        });
        jPanel1.add(NuevoJuego);
        NuevoJuego.setBounds(80, 0, 330, 70);

        jButton3.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        jButton3.setText("Partidas");
        jButton3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);
        jButton3.setBounds(80, 130, 330, 70);

        salir.setBackground(new java.awt.Color(204, 204, 204));
        salir.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/salir_opt.png"))); // NOI18N
        salir.setToolTipText("Salir");
        salir.setBorder(null);
        salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        jPanel1.add(salir);
        salir.setBounds(380, 260, 70, 70);

        atras3.setBackground(new java.awt.Color(204, 204, 204));
        atras3.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        atras3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cerrar_opt.png"))); // NOI18N
        atras3.setToolTipText("Cerrar Sesion");
        atras3.setBorder(null);
        atras3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        atras3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                atras3ActionPerformed(evt);
            }
        });
        jPanel1.add(atras3);
        atras3.setBounds(50, 260, 70, 70);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 240, 500, 440);

        configurar.setBackground(new java.awt.Color(255, 255, 255));
        configurar.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        configurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/nuevo-gif-1-iloveimg-resized (1).gif"))); // NOI18N
        configurar.setToolTipText("Configurar Cuenta");
        configurar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        configurar.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        configurar.setIconTextGap(1);
        configurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                configurarActionPerformed(evt);
            }
        });
        getContentPane().add(configurar);
        configurar.setBounds(410, 10, 80, 70);

        jPanel2.setBackground(new java.awt.Color(153, 153, 153));
        jPanel2.setLayout(null);

        Personas.setBackground(new java.awt.Color(102, 102, 102));
        Personas.setFont(new java.awt.Font("Magneto", 1, 24)); // NOI18N
        Personas.setForeground(new java.awt.Color(0, 0, 0));
        Personas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2 Personas", "3 Personas", "4 Personas" }));
        Personas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PersonasActionPerformed(evt);
            }
        });
        jPanel2.add(Personas);
        Personas.setBounds(490, 200, 180, 40);

        jLabel1.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(181, 28, 28));
        jLabel1.setText("Número de Jugadores:");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(30, 200, 450, 40);

        jLabel5.setBackground(new java.awt.Color(76, 76, 76));
        jLabel5.setFont(new java.awt.Font("Magneto", 1, 55)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(204, 204, 204));
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Imagen-animada-Domino-05-iloveimg-resized.gif"))); // NOI18N
        jLabel5.setText("Nuevo Juego");
        jLabel5.setIconTextGap(30);
        jPanel2.add(jLabel5);
        jLabel5.setBounds(110, 80, 461, 62);

        Oponentes.setEditable(false);
        Oponentes.setBackground(new java.awt.Color(51, 51, 51));
        Oponentes.setColumns(3);
        Oponentes.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        Oponentes.setForeground(new java.awt.Color(204, 204, 204));
        Oponentes.setRows(3);
        jScrollPane1.setViewportView(Oponentes);

        jPanel2.add(jScrollPane1);
        jScrollPane1.setBounds(460, 370, 150, 150);

        seleccionarOponente.setFont(new java.awt.Font("Gadugi", 1, 24)); // NOI18N
        seleccionarOponente.setForeground(new java.awt.Color(204, 204, 204));
        seleccionarOponente.setText("Selecione su oponente:");
        jPanel2.add(seleccionarOponente);
        seleccionarOponente.setBounds(40, 310, 370, 30);

        jLabel7.setBackground(new java.awt.Color(0, 0, 0));
        jLabel7.setFont(new java.awt.Font("Magneto", 1, 30)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(173, 41, 41));
        jLabel7.setText("Oponente");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(150, 350, 170, 50);

        Oponente.setFont(new java.awt.Font("Magneto", 1, 24)); // NOI18N
        Oponente.setForeground(new java.awt.Color(182, 33, 33));
        Oponente.setText("Oponente");
        jPanel2.add(Oponente);
        Oponente.setBounds(470, 310, 140, 50);

        SeleccionarOponentes.setEditable(false);
        SeleccionarOponentes.setBackground(new java.awt.Color(51, 51, 51));
        SeleccionarOponentes.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        SeleccionarOponentes.setForeground(new java.awt.Color(204, 204, 204));
        SeleccionarOponentes.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        SeleccionarOponentes.setText("Nombre");
        jPanel2.add(SeleccionarOponentes);
        SeleccionarOponentes.setBounds(90, 400, 270, 40);

        siguiente.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        siguiente.setText(">>");
        siguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                siguienteActionPerformed(evt);
            }
        });
        jPanel2.add(siguiente);
        siguiente.setBounds(300, 450, 60, 40);

        anterior.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        anterior.setText("<<");
        anterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                anteriorActionPerformed(evt);
            }
        });
        jPanel2.add(anterior);
        anterior.setBounds(90, 450, 60, 40);

        agregarOponente.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        agregarOponente.setText("Agregar");
        agregarOponente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarOponenteActionPerformed(evt);
            }
        });
        jPanel2.add(agregarOponente);
        agregarOponente.setBounds(160, 450, 130, 40);

        Jugar.setBackground(new java.awt.Color(0, 0, 0));
        Jugar.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        Jugar.setForeground(new java.awt.Color(51, 255, 51));
        Jugar.setText("Iniciar Juego");
        Jugar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JugarActionPerformed(evt);
            }
        });
        jPanel2.add(Jugar);
        Jugar.setBounds(160, 550, 370, 60);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fonde_Juego_opt.jpg"))); // NOI18N
        jPanel2.add(jLabel4);
        jLabel4.setBounds(0, 0, 700, 680);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(500, 0, 700, 680);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/giphy.gif"))); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 0, 500, 280);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        atras();
    }//GEN-LAST:event_formWindowClosing

    private void PersonasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PersonasActionPerformed
        InsertarOponentesEnLista();
        OponentesSeleccionados.clear(); 
        ImprimirOponentes();
        
        String NumeroPersonas = Personas.getSelectedItem().toString();
        switch (NumeroPersonas) {
            case "4 Personas":
                Oponente.setText("Oponentes");
                seleccionarOponente.setText("Seleccione tres oponentes:");
                break;
            case "3 Personas":
                Oponente.setText("Oponentes");
                seleccionarOponente.setText("Seleccione dos oponentes:");
                break;
            default:
                Oponente.setText("Oponente");
                seleccionarOponente.setText("Seleccione su oponente:");
                break;
        }
    }//GEN-LAST:event_PersonasActionPerformed

    private void agregarOponenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregarOponenteActionPerformed
        Usuario oponente = listaOponentes.get(oponenteSeleccionado);
        switch (Personas.getSelectedItem().toString()) {
            
            case "4 Personas":
                if (OponentesSeleccionados.size() != 3){
                    OponentesSeleccionados.add(oponente);
                    ImprimirOponentes();
                    validarLabel();
                }
                else
                    JOptionPane.showMessageDialog(null, "Solo se pueden agregar tres oponentes...!!");
                break;
                
            case "3 Personas":
                if (OponentesSeleccionados.size() != 2){
                    OponentesSeleccionados.add(oponente);
                    ImprimirOponentes();
                    validarLabel();
                }
                else
                    JOptionPane.showMessageDialog(null, "Solo se pueden agregar dos oponentes...!!");
                break;
                
            default:
                OponentesSeleccionados.clear();
                OponentesSeleccionados.add(oponente);
                ImprimirOponentes();
                break;
        }
    }//GEN-LAST:event_agregarOponenteActionPerformed

    private void siguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_siguienteActionPerformed
        if (oponenteSeleccionado != listaOponentes.size()-1){
            oponenteSeleccionado+=1;
            SeleccionarOponentes.setText(listaOponentes.get(oponenteSeleccionado).nombre);
        }
        else{
            oponenteSeleccionado=0;
            SeleccionarOponentes.setText(listaOponentes.get(oponenteSeleccionado).nombre);
        }
    }//GEN-LAST:event_siguienteActionPerformed

    private void anteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_anteriorActionPerformed
        if (oponenteSeleccionado != 0){
            oponenteSeleccionado-=1;
            SeleccionarOponentes.setText(listaOponentes.get(oponenteSeleccionado).nombre);
        }
        else{
            oponenteSeleccionado = listaOponentes.size()-1;
            SeleccionarOponentes.setText(listaOponentes.get(oponenteSeleccionado).nombre);
        }
    }//GEN-LAST:event_anteriorActionPerformed

    private void JugarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JugarActionPerformed
        String NumeroPersonas = Personas.getSelectedItem().toString();
        switch (NumeroPersonas) {
            case "4 Personas":
                if (OponentesSeleccionados.size()==3){
                   UsuarioConfirmar3 frame = new UsuarioConfirmar3(login, met, this);
                    frame.setVisible(true);
                }
                else
                    JOptionPane.showMessageDialog(null, "Debe tener seleccionados tres oponentes...!!");
                break;
            case "3 Personas":
                if (OponentesSeleccionados.size()==2){
                    UsuarioConfirmar2 frame = new UsuarioConfirmar2(login, met, this);
                    frame.setVisible(true);
                }
                else
                    JOptionPane.showMessageDialog(null, "Debe tener seleccionados dos oponentes...!!");
                break;
            default:
                if (OponentesSeleccionados.size()==1){
                    UsuarioConfirmar1 frame = new UsuarioConfirmar1(login, met, this);
                    frame.setVisible(true);
                }
                else
                    JOptionPane.showMessageDialog(null, "Debe tener seleccionado un oponente...!!");
                break;
        }
    }//GEN-LAST:event_JugarActionPerformed

    private void configurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_configurarActionPerformed
        UsuarioConfiguracion frame = new UsuarioConfiguracion(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_configurarActionPerformed

    private void NuevoJuegoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NuevoJuegoActionPerformed
        UsuarioNuevoJuego frame = new UsuarioNuevoJuego(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_NuevoJuegoActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        UsuarioPartidas frame = new UsuarioPartidas(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        this.dispose();
        login.dispose();
    }//GEN-LAST:event_salirActionPerformed

    private void atras3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_atras3ActionPerformed
        atras();
    }//GEN-LAST:event_atras3ActionPerformed

    public void InsertarOponentesEnLista(){
        listaOponentes.clear();
        Usuario aux = met.inicio;
        while (aux != met.fin){
            if (!aux.cedula.equals(login.usuario.cedula))
                listaOponentes.add(aux);
            aux = aux.sig;
        }
        if (!aux.cedula.equals(login.usuario.cedula))
            listaOponentes.add(aux);
        
        for(int i=0; i<listaOponentes.size(); i++)
            if (listaOponentes.get(i).cedula.equals("2017102058"))
                listaOponentes.remove(i);
    }
    
    public void ImprimirOponentes(){
        String oponentes = "";
        for(int i=0; i<OponentesSeleccionados.size(); i++){
            oponentes+=OponentesSeleccionados.get(i).nombre+"\n";
        }
        Oponentes.setText(oponentes);
    }

    public void atras(){
        login.setVisible(true);
        this.dispose();
    }
    
    public void validarLabel(){
        listaOponentes.remove(oponenteSeleccionado);
        if (oponenteSeleccionado != listaOponentes.size()){
            SeleccionarOponentes.setText(listaOponentes.get(oponenteSeleccionado).nombre);
        }
        else{
            oponenteSeleccionado=0;
            SeleccionarOponentes.setText(listaOponentes.get(oponenteSeleccionado).nombre);
        }
    }
    /**
     * @param args the command line arguments
     */
   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Jugar;
    private javax.swing.JButton NuevoJuego;
    private javax.swing.JLabel Oponente;
    private javax.swing.JTextArea Oponentes;
    private javax.swing.JComboBox<String> Personas;
    private javax.swing.JTextField SeleccionarOponentes;
    private javax.swing.JButton agregarOponente;
    private javax.swing.JButton anterior;
    private javax.swing.JButton atras3;
    private javax.swing.JButton configurar;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton salir;
    private javax.swing.JLabel seleccionarOponente;
    private javax.swing.JButton siguiente;
    private javax.swing.JLabel usuario;
    // End of variables declaration//GEN-END:variables
}
