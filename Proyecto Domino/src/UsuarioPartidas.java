
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class UsuarioPartidas extends javax.swing.JFrame {
    Login login;
    Metodos met;
    Partida p ;
    
    public UsuarioPartidas(Login login,  Metodos met) {
        this.login = login;
        this.met = met;
        this.setTitle("Partidas");
        this.setResizable(false);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/descarga.png")).getImage());
        initComponents();
        usuario.setText(String.valueOf(login.usuario.nombre));
        p = login.usuario.InicioPartida;
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        usuario = new javax.swing.JLabel();
        configurar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        NuevoJuego = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        salir = new javax.swing.JButton();
        atras3 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        Cargar = new javax.swing.JButton();
        eliminar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setLocation(new java.awt.Point(400, 200));
        setMaximumSize(new java.awt.Dimension(1200, 700));
        setMinimumSize(new java.awt.Dimension(1200, 700));
        setPreferredSize(new java.awt.Dimension(1200, 700));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(null);

        jLabel3.setFont(new java.awt.Font("Magneto", 1, 48)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Main");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(20, 10, 140, 50);

        usuario.setFont(new java.awt.Font("Dialog", 3, 24)); // NOI18N
        usuario.setForeground(new java.awt.Color(255, 255, 255));
        usuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        usuario.setText("Nombre");
        usuario.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(usuario);
        usuario.setBounds(290, 10, 110, 40);

        configurar.setBackground(new java.awt.Color(255, 255, 255));
        configurar.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        configurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/nuevo-gif-1-iloveimg-resized (1).gif"))); // NOI18N
        configurar.setToolTipText("Configurar Cuenta");
        configurar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        configurar.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        configurar.setIconTextGap(1);
        configurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                configurarActionPerformed(evt);
            }
        });
        getContentPane().add(configurar);
        configurar.setBounds(410, 10, 80, 70);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));
        jPanel1.setLayout(null);

        NuevoJuego.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        NuevoJuego.setText("Nuevo Juego");
        NuevoJuego.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        NuevoJuego.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NuevoJuegoActionPerformed(evt);
            }
        });
        jPanel1.add(NuevoJuego);
        NuevoJuego.setBounds(80, 0, 330, 70);

        jButton3.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        jButton3.setText("Partidas");
        jButton3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);
        jButton3.setBounds(80, 130, 330, 70);

        salir.setBackground(new java.awt.Color(204, 204, 204));
        salir.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/salir_opt.png"))); // NOI18N
        salir.setToolTipText("Salir");
        salir.setBorder(null);
        salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        jPanel1.add(salir);
        salir.setBounds(380, 260, 70, 70);

        atras3.setBackground(new java.awt.Color(204, 204, 204));
        atras3.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        atras3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cerrar_opt.png"))); // NOI18N
        atras3.setToolTipText("Cerrar Sesion");
        atras3.setBorder(null);
        atras3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        atras3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                atras3ActionPerformed(evt);
            }
        });
        jPanel1.add(atras3);
        atras3.setBounds(50, 260, 70, 70);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 240, 500, 440);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/giphy.gif"))); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 0, 500, 280);

        jLabel5.setBackground(new java.awt.Color(76, 76, 76));
        jLabel5.setFont(new java.awt.Font("Magneto", 1, 55)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(204, 204, 204));
        jLabel5.setText("Partidas");
        jLabel5.setIconTextGap(30);
        getContentPane().add(jLabel5);
        jLabel5.setBounds(730, 70, 270, 62);

        Cargar.setBackground(new java.awt.Color(0, 0, 0));
        Cargar.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        Cargar.setForeground(new java.awt.Color(51, 255, 51));
        Cargar.setText("Cargar Partida");
        Cargar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CargarActionPerformed(evt);
            }
        });
        getContentPane().add(Cargar);
        Cargar.setBounds(630, 260, 420, 70);

        eliminar.setBackground(new java.awt.Color(0, 0, 0));
        eliminar.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        eliminar.setForeground(new java.awt.Color(51, 255, 51));
        eliminar.setText("Eliminar Partida");
        eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminarActionPerformed(evt);
            }
        });
        getContentPane().add(eliminar);
        eliminar.setBounds(640, 410, 420, 70);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fonde_Juego_opt.jpg"))); // NOI18N
        getContentPane().add(jLabel4);
        jLabel4.setBounds(500, 0, 700, 680);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void configurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_configurarActionPerformed
        UsuarioConfiguracion frame = new UsuarioConfiguracion(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_configurarActionPerformed

    private void NuevoJuegoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NuevoJuegoActionPerformed
        UsuarioNuevoJuego frame = new UsuarioNuevoJuego(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_NuevoJuegoActionPerformed

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        this.dispose();
        login.dispose();
    }//GEN-LAST:event_salirActionPerformed

    private void atras3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_atras3ActionPerformed
        atras();
    }//GEN-LAST:event_atras3ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        atras();
    }//GEN-LAST:event_formWindowClosing

    private void CargarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CargarActionPerformed
        
        if (p != null){
            JFrame frameJuego = new JFrame();
            frameJuego.setLocation(10, 10);
            frameJuego.setSize(1900,1050);
            frameJuego.setResizable(false);
            frameJuego.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/descarga.png")).getImage());
            switch (p.jugadores) {
                case 2:
                    boolean juegoYo;
                    juegoYo = p.JuegoYo == 1;
                    CargarPartida CargarPartida = new CargarPartida(met, login, p.seleccionada, p.misFichas, p.fichasOponente, p.iniciojuego, p.finJuego, juegoYo, p.trampa1, p.trampa2, p.Vuelta);
                    frameJuego.add(CargarPartida);
                    frameJuego.setVisible(true);
                    break;
                case 3:
                    CargarPartida2 CargarPartida2 = new CargarPartida2(met, login, p.seleccionada, p.misFichas, p.fichasOponente, p.fichasOponente2, p.iniciojuego, p.finJuego, p.JuegoYo, p.trampa1, p.trampa2, p.trampa3, p.Vuelta);
                    frameJuego.add(CargarPartida2);
                    frameJuego.setVisible(true);
                    break;
                default:
                    CargarPartida3 CargarPartida3 = new CargarPartida3(met, login, p.seleccionada, p.misFichas, p.fichasOponente, p.fichasOponente2, p.fichasOponente3, p.iniciojuego, p.finJuego, p.JuegoYo, p.trampa1, p.trampa2, p.trampa3, p.trampa4, p.Vuelta);
                    frameJuego.add(CargarPartida3);
                    frameJuego.setVisible(true);
                    break;
            }
        }
        else
            JOptionPane.showMessageDialog(null, "No hay partidas guardadas...!!");
    }//GEN-LAST:event_CargarActionPerformed

    private void eliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminarActionPerformed
        login.usuario.InicioPartida = login.usuario.InicioPartida.sig;
        JOptionPane.showMessageDialog(null, "Eliminar partida...!!");
    }//GEN-LAST:event_eliminarActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        UsuarioPartidas frame = new UsuarioPartidas(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    public void atras(){
        login.setVisible(true);
        this.dispose();
    }
    /**
     * @param args the command line arguments
     */
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Cargar;
    private javax.swing.JButton NuevoJuego;
    private javax.swing.JButton atras3;
    private javax.swing.JButton configurar;
    private javax.swing.JButton eliminar;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton salir;
    private javax.swing.JLabel usuario;
    // End of variables declaration//GEN-END:variables
}