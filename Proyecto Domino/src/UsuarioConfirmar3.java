
import javax.swing.ImageIcon;
import javax.swing.JFrame;


public class UsuarioConfirmar3 extends javax.swing.JFrame {
    Login login;
    Metodos met;
    UsuarioNuevoJuego NJ;
    
    public UsuarioConfirmar3(Login login,  Metodos met, UsuarioNuevoJuego NJ) {
        this.login = login;
        this.met = met;
        this.NJ = NJ;
        this.setTitle("Contraseña del Oponente");
        this.setResizable(false);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/descarga.png")).getImage());
        initComponents();
        nombre1.setText(NJ.OponentesSeleccionados.get(0).nombre);
        nombre2.setText(NJ.OponentesSeleccionados.get(1).nombre);
        nombre3.setText(NJ.OponentesSeleccionados.get(2).nombre);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Contrasena2 = new javax.swing.JPasswordField();
        Contrasena1 = new javax.swing.JPasswordField();
        Contrasena3 = new javax.swing.JPasswordField();
        jLabel1 = new javax.swing.JLabel();
        Jugar = new javax.swing.JButton();
        Jugar1 = new javax.swing.JButton();
        nombre1 = new javax.swing.JLabel();
        nombre2 = new javax.swing.JLabel();
        nombre3 = new javax.swing.JLabel();
        check1 = new javax.swing.JLabel();
        check2 = new javax.swing.JLabel();
        check3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setLocation(new java.awt.Point(710, 280));
        setMaximumSize(new java.awt.Dimension(700, 500));
        setMinimumSize(new java.awt.Dimension(700, 500));
        setPreferredSize(new java.awt.Dimension(700, 500));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(null);

        Contrasena2.setBackground(new java.awt.Color(51, 51, 51));
        Contrasena2.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Contrasena2.setForeground(new java.awt.Color(204, 204, 204));
        Contrasena2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        Contrasena2.setToolTipText("contraseña");
        getContentPane().add(Contrasena2);
        Contrasena2.setBounds(270, 220, 140, 40);

        Contrasena1.setBackground(new java.awt.Color(51, 51, 51));
        Contrasena1.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Contrasena1.setForeground(new java.awt.Color(204, 204, 204));
        Contrasena1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        Contrasena1.setToolTipText("contraseña");
        getContentPane().add(Contrasena1);
        Contrasena1.setBounds(50, 220, 140, 40);

        Contrasena3.setBackground(new java.awt.Color(51, 51, 51));
        Contrasena3.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Contrasena3.setForeground(new java.awt.Color(204, 204, 204));
        Contrasena3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        Contrasena3.setToolTipText("contraseña");
        getContentPane().add(Contrasena3);
        Contrasena3.setBounds(490, 220, 140, 40);

        jLabel1.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 204, 204));
        jLabel1.setText("Contraseña de los Oponentes");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(50, 60, 580, 60);

        Jugar.setBackground(new java.awt.Color(0, 0, 0));
        Jugar.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        Jugar.setForeground(new java.awt.Color(51, 255, 51));
        Jugar.setText("Aceptar");
        Jugar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JugarActionPerformed(evt);
            }
        });
        getContentPane().add(Jugar);
        Jugar.setBounds(390, 330, 210, 60);

        Jugar1.setBackground(new java.awt.Color(0, 0, 0));
        Jugar1.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        Jugar1.setForeground(new java.awt.Color(166, 31, 31));
        Jugar1.setText("Cancelar");
        Jugar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jugar1ActionPerformed(evt);
            }
        });
        getContentPane().add(Jugar1);
        Jugar1.setBounds(80, 330, 210, 60);

        nombre1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        nombre1.setForeground(new java.awt.Color(204, 204, 204));
        nombre1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        nombre1.setText("Nombre");
        getContentPane().add(nombre1);
        nombre1.setBounds(60, 170, 130, 30);

        nombre2.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        nombre2.setForeground(new java.awt.Color(204, 204, 204));
        nombre2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        nombre2.setText("Nombre");
        getContentPane().add(nombre2);
        nombre2.setBounds(280, 170, 130, 30);

        nombre3.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        nombre3.setForeground(new java.awt.Color(204, 204, 204));
        nombre3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        nombre3.setText("Nombre");
        getContentPane().add(nombre3);
        nombre3.setBounds(490, 170, 130, 30);
        getContentPane().add(check1);
        check1.setBounds(200, 220, 41, 40);
        getContentPane().add(check2);
        check2.setBounds(420, 220, 41, 40);
        getContentPane().add(check3);
        check3.setBounds(640, 220, 41, 40);

        jLabel4.setMaximumSize(new java.awt.Dimension(600, 500));
        jLabel4.setMinimumSize(new java.awt.Dimension(600, 500));
        jLabel4.setPreferredSize(new java.awt.Dimension(600, 500));
        getContentPane().add(jLabel4);
        jLabel4.setBounds(0, 0, 700, 480);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fonde_Juego_opt - copia.jpg"))); // NOI18N
        jLabel5.setMaximumSize(new java.awt.Dimension(600, 500));
        jLabel5.setMinimumSize(new java.awt.Dimension(600, 500));
        jLabel5.setPreferredSize(new java.awt.Dimension(600, 500));
        getContentPane().add(jLabel5);
        jLabel5.setBounds(0, 0, 720, 480);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void JugarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JugarActionPerformed
        String contrasena1 = "";
        char []  contra1 = Contrasena1.getPassword();
        for(int i=0; i<contra1.length; i++) contrasena1 += contra1[i];

        String contrasena2 = "";
        char []  contra2 = Contrasena2.getPassword();
        for(int i=0; i<contra2.length; i++) contrasena2 += contra2[i];
        
        String contrasena3 = "";
        char []  contra3 = Contrasena3.getPassword();
        for(int i=0; i<contra3.length; i++) contrasena3 += contra3[i];

        boolean x = met.InicioDeSesion(NJ.OponentesSeleccionados.get(0).cedula, contrasena1);
        boolean y = met.InicioDeSesion(NJ.OponentesSeleccionados.get(1).cedula, contrasena2);
        boolean z = met.InicioDeSesion(NJ.OponentesSeleccionados.get(2).cedula, contrasena3);

        if (x == true){
            ImageIcon icon = new ImageIcon(this.getClass().getResource("/Imagenes/bueno_opt.png"));
            check1.setIcon(icon);
        }
        else{
            ImageIcon icon = new ImageIcon(this.getClass().getResource("/Imagenes/bmalo_opt.png"));
            check1.setIcon(icon);
        }

        if (y == true){
            ImageIcon icon = new ImageIcon(this.getClass().getResource("/Imagenes/bueno_opt.png"));
            check2.setIcon(icon);
        }
        else{
            ImageIcon icon = new ImageIcon(this.getClass().getResource("/Imagenes/bmalo_opt.png"));
            check2.setIcon(icon);
        }
        
        if (z == true){
            ImageIcon icon = new ImageIcon(this.getClass().getResource("/Imagenes/bueno_opt.png"));
            check3.setIcon(icon);
        }
        else{
            ImageIcon icon = new ImageIcon(this.getClass().getResource("/Imagenes/bmalo_opt.png"));
            check3.setIcon(icon);
        }

        if(x == true && y == true && z == true){
            this.dispose();
            met.CrearFichas();
            Juego3 jugar = new Juego3(met, login);
                
            JFrame frameJuego = new JFrame();
            frameJuego.setLocation(10, 10);
            frameJuego.setSize(1900,1050);
//            frameJuego.setTitle("Juego 1 vs 1");
            frameJuego.setResizable(false);
            frameJuego.add(jugar);
            frameJuego.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/descarga.png")).getImage());
            frameJuego.setVisible(true);
        }
    }//GEN-LAST:event_JugarActionPerformed

    private void Jugar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jugar1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_Jugar1ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        this.dispose();
    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField Contrasena1;
    private javax.swing.JPasswordField Contrasena2;
    private javax.swing.JPasswordField Contrasena3;
    private javax.swing.JButton Jugar;
    private javax.swing.JButton Jugar1;
    private javax.swing.JLabel check1;
    private javax.swing.JLabel check2;
    private javax.swing.JLabel check3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel nombre1;
    private javax.swing.JLabel nombre2;
    private javax.swing.JLabel nombre3;
    // End of variables declaration//GEN-END:variables
}
