
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.ImageIcon;

public class VerPartidasCreadas extends Canvas implements MouseListener, KeyListener{
    int jugadores;
    Metodos met;
    Login login;
    Partida p;
    
    Ficha seleccionada;
    
    ArrayList<Ficha> misFichas = new ArrayList<>();
    ArrayList<Ficha> fichasOponente = new ArrayList<>();
    ArrayList<Ficha> fichasOponente2 = new ArrayList<>();
    ArrayList<Ficha> fichasOponente3 = new ArrayList<>();
    
    Ficha Inicio;
    Ficha Fin;
    
    Calendar calendario = Calendar.getInstance();
    int hora =calendario.get(Calendar.HOUR_OF_DAY);
    int minuto = calendario.get(Calendar.MINUTE);
    int segundo = calendario.get(Calendar.SECOND);
    
    int JuegoYo;
    boolean trampa1 = true;
    boolean trampa2 = true;
    boolean trampa3 = true;
    boolean trampa4 = true;

    int Vuelta = 1;
    
    int PierdeTurno = 0;
    
    public VerPartidasCreadas(Partida p, Metodos met, Login login, Ficha seleccionada, ArrayList<Ficha> misFichas, ArrayList<Ficha> fichasOponente, Ficha iniciojuego, Ficha finJuego, int JuegoYo, boolean trampa1, boolean trampa2, int Vuelta) {  
        jugadores = 2;
        this.p = p;
        this.met = met;
        this.login = login;
        this.seleccionada = seleccionada;
        this.misFichas = misFichas;
        this.fichasOponente = fichasOponente;
        this.Inicio = iniciojuego;
        this.Fin = finJuego;
        this.JuegoYo = JuegoYo;
        this.trampa1 = trampa1;
        this.trampa2 = trampa2;
        this.Vuelta = Vuelta;
    }
    
    public VerPartidasCreadas(Partida p, Metodos met, Login login, Ficha seleccionada, ArrayList<Ficha> misFichas, ArrayList<Ficha> fichasOponente, ArrayList<Ficha> fichasOponente2, Ficha iniciojuego, Ficha finJuego, int JuegoYo, boolean trampa1, boolean trampa2, boolean trampa3, int Vuelta) {
        jugadores = 3;
        this.p = p;
        this.login = login;
        this.met = met;
        this.seleccionada = seleccionada;
        this.misFichas = misFichas;
        this.fichasOponente = fichasOponente;
        this.fichasOponente2 = fichasOponente2;
        this.Inicio = iniciojuego;
        this.Fin = finJuego;
        this.JuegoYo = JuegoYo;
        this.trampa1 = trampa1;
        this.trampa2 = trampa2;
        this.trampa3 = trampa3;
        this.Vuelta = Vuelta;
    }
    
    public VerPartidasCreadas(Partida p, Metodos met, Login login, Ficha seleccionada, ArrayList<Ficha> misFichas, ArrayList<Ficha> fichasOponente, ArrayList<Ficha> fichasOponente2, ArrayList<Ficha> fichasOponente3, Ficha iniciojuego, Ficha finJuego, int JuegoYo, boolean trampa1, boolean trampa2, boolean trampa3, boolean trampa4, int Vuelta) {
        jugadores = 4;
        this.p = p;
        this.login = login;
        this.met = met;
        this.seleccionada = seleccionada;
        this.misFichas = misFichas;
        this.fichasOponente = fichasOponente;
        this.fichasOponente2 = fichasOponente2;
        this.fichasOponente3 = fichasOponente3;
        this.Inicio = iniciojuego;
        this.Fin = finJuego;
        this.JuegoYo = JuegoYo;
        this.trampa1 = trampa1;
        this.trampa2 = trampa2;
        this.trampa3 = trampa3;
        this.trampa4 = trampa4;
        this.Vuelta = Vuelta;
    }
    
    public void paint(Graphics g) {
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/fonde_Juego_opt - copia.jpg")).getImage(), 30, 170, 1630, 650, null);
        if (jugadores == 2)
            paint1(g);
        if (jugadores == 3)
            paint2(g);
        if (jugadores == 4)
            paint3(g);
    }
    
    public void paint1(Graphics g) {
        if (JuegoYo == 1){
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Su turno.png")).getImage(), 1500, 870, null);
            if (trampa1){
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Retire ficha.png")).getImage(), 1700, 50, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Un turno.png")).getImage(), 1700, 90, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Ficha nueva.png")).getImage(), 1700, 130, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Trampas.png")).getImage(), 1690, 3, null);
            }
        }
        if (JuegoYo == 2){
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Su turno.png")).getImage(), 1500, 50, null);
            if (trampa2){
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Retire ficha.png")).getImage(), 1700, 50, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Un turno.png")).getImage(), 1700, 90, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Ficha nueva.png")).getImage(), 1700, 130, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Trampas.png")).getImage(), 1690, 3, null);
            }
        }
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Preciona Click Derecho.png")).getImage(), 30, 830, null);
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/TomarFicha.png")).getImage(), 1680, 200, null);
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/pasar_opt.png")).getImage(), 1715, 830, null);
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/guardar_opt.png")).getImage(), 1580, 180, null);
        
        int x = 30;
        for (int i=0; i<misFichas.size(); i++){
            misFichas.get(i).x = x;
            misFichas.get(i).y = 865;
            x += 75;
            g.drawImage(misFichas.get(i).imagen1.getImage(), misFichas.get(i).x, misFichas.get(i).y, 72, 144, null);
        }
        
        x = 30;
        for (int i=0; i<fichasOponente.size(); i++){
            fichasOponente.get(i).x = x;
            fichasOponente.get(i).y = 10;
            x += 75;
            g.drawImage(fichasOponente.get(i).imagen1.getImage(), fichasOponente.get(i).x, fichasOponente.get(i).y, 72, 144, null);
        }
        
        imprimirJuego(g);
        
        if(seleccionada!=null){
            if (Vuelta == 1 || Vuelta == 3){
                seleccionada.ancho = 72;
                seleccionada.largo = 144;
            }
            else{
                seleccionada.ancho = 144;
                seleccionada.largo = 72;
            }
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Preciona Espacio.png")).getImage(), 1680, 500, null);
            imprimirSeleccionada(g);
        }
    }
    
    public void paint2(Graphics g) {
        if (JuegoYo == 1){
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Su turno.png")).getImage(), 900, 870, null);
            if (trampa1){
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Retire ficha.png")).getImage(), 1710, 300, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Un turno.png")).getImage(), 1710, 340, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Ficha nueva.png")).getImage(), 1710, 380, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Trampas.png")).getImage(), 1700, 253, null);
            }
        }
        if (JuegoYo == 2){
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Su turno.png")).getImage(), 900, 50, null);
            if (trampa2){
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Retire ficha.png")).getImage(), 1710, 300, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Un turno.png")).getImage(), 1710, 340, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Ficha nueva.png")).getImage(), 1710, 380, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Trampas.png")).getImage(), 1700, 253, null);
            }
        }
        if (JuegoYo == 3){
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Su turno2.png")).getImage(), 900, 50, null);
            if (trampa3){
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Retire ficha.png")).getImage(), 1710, 300, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Un turno.png")).getImage(), 1710, 340, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Ficha nueva.png")).getImage(), 1710, 380, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Trampas.png")).getImage(), 1700, 253, null);
            }
        }
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Preciona Click Derecho.png")).getImage(), 30, 830, null);
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/TomarFicha.png")).getImage(), 1420, 820, null);
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/pasar_opt.png")).getImage(), 1715, 830, null);
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/guardar_opt.png")).getImage(), 1580, 180, null);
        
        int x = 30;
        for (int i=0; i<misFichas.size(); i++){
            misFichas.get(i).x = x;
            misFichas.get(i).y = 865;
            x += 75;
            g.drawImage(misFichas.get(i).imagen1.getImage(), misFichas.get(i).x, misFichas.get(i).y, 72, 144, null);
        }
        
        x = 30;
        for (int i=0; i<fichasOponente.size(); i++){
            fichasOponente.get(i).x = x;
            fichasOponente.get(i).y = 10;
            x += 75;
            g.drawImage(fichasOponente.get(i).imagen1.getImage(), fichasOponente.get(i).x, fichasOponente.get(i).y, 72, 144, null);
        }
        
        x = 1790;
        for (int i=0; i<fichasOponente2.size(); i++){
            fichasOponente2.get(i).x = x;
            fichasOponente2.get(i).y = 10;
            x -= 75;
            g.drawImage(fichasOponente2.get(i).imagen1.getImage(), fichasOponente2.get(i).x, fichasOponente2.get(i).y, 72, 144, null);
        }
        
        imprimirJuego(g);
        
        if(seleccionada!=null){
            if (Vuelta == 1 || Vuelta == 3){
                seleccionada.ancho = 72;
                seleccionada.largo = 144;
            }
            else{
                seleccionada.ancho = 144;
                seleccionada.largo = 72;
            }
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Preciona Espacio.png")).getImage(), 1680, 500, null);
            imprimirSeleccionada(g);
        }
    }
    
    public void paint3(Graphics g) {
        if (JuegoYo == 1){
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Su turno.png")).getImage(), 900, 870, null);
            if (trampa1){
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Retire ficha.png")).getImage(), 1710, 300, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Un turno.png")).getImage(), 1710, 340, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Trampas.png")).getImage(), 1700, 253, null);
            }
        }
        if (JuegoYo == 2){
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Su turno.png")).getImage(), 550, 50, null);
            if (trampa2){
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Retire ficha.png")).getImage(), 1710, 300, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Un turno.png")).getImage(), 1710, 340, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Trampas.png")).getImage(), 1700, 253, null);
            }
        }
        if (JuegoYo == 3){
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Su turno2.png")).getImage(), 560, 50, null);
            if (trampa3){
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Retire ficha.png")).getImage(), 1710, 300, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Un turno.png")).getImage(), 1710, 340, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Trampas.png")).getImage(), 1700, 253, null);
            }
        }
        if (JuegoYo == 4){
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Su turno2.png")).getImage(), 1220, 50, null);
            if (trampa4){
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Retire ficha.png")).getImage(), 1710, 300, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Un turno.png")).getImage(), 1710, 340, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Trampas.png")).getImage(), 1700, 253, null);
            }
        }
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Preciona Click Derecho.png")).getImage(), 30, 830, null);
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/pasar_opt.png")).getImage(), 1715, 830, null);
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/guardar_opt.png")).getImage(), 1580, 180, null);
        
        int x = 30;
        for (int i=0; i<misFichas.size(); i++){
            misFichas.get(i).x = x;
            misFichas.get(i).y = 865;
            x += 75;
            g.drawImage(misFichas.get(i).imagen1.getImage(), misFichas.get(i).x, misFichas.get(i).y, 72, 144, null);
        }
        
        x = 20;
        for (int i=0; i<fichasOponente.size(); i++){
            fichasOponente.get(i).x = x;
            fichasOponente.get(i).y = 10;
            x += 75;
            g.drawImage(fichasOponente.get(i).imagen1.getImage(), fichasOponente.get(i).x, fichasOponente.get(i).y, 72, 144, null);
        }
        
        x = 680;
        for (int i=0; i<fichasOponente2.size(); i++){
            fichasOponente2.get(i).x = x;
            fichasOponente2.get(i).y = 10;
            x += 75;
            g.drawImage(fichasOponente2.get(i).imagen1.getImage(), fichasOponente2.get(i).x, fichasOponente2.get(i).y, 72, 144, null);
        }
        
        x = 1350;
        for (int i=0; i<fichasOponente3.size(); i++){
            fichasOponente3.get(i).x = x;
            fichasOponente3.get(i).y = 10;
            x += 75;
            g.drawImage(fichasOponente3.get(i).imagen1.getImage(), fichasOponente3.get(i).x, fichasOponente3.get(i).y, 72, 144, null);
        }
        
        imprimirJuego(g);
        
        if(seleccionada!=null){
            if (Vuelta == 1 || Vuelta == 3){
                seleccionada.ancho = 72;
                seleccionada.largo = 144;
            }
            else{
                seleccionada.ancho = 144;
                seleccionada.largo = 72;
            }
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Preciona Espacio.png")).getImage(), 1680, 500, null);
            imprimirSeleccionada(g);
        }
    }
    
    public void imprimirSeleccionada(Graphics g){ 
        if(Vuelta==1)
            g.drawImage(seleccionada.imagen1.getImage(), 1740, 590, seleccionada.ancho, seleccionada.largo, null);
        if(Vuelta==2)
            g.drawImage(seleccionada.imagen2.getImage(), 1704, 626, seleccionada.ancho, seleccionada.largo, null);
        if(Vuelta==3)
            g.drawImage(seleccionada.imagen3.getImage(), 1740, 590, seleccionada.ancho, seleccionada.largo, null);
        if(Vuelta==4)
            g.drawImage(seleccionada.imagen4.getImage(), 1704, 626, seleccionada.ancho, seleccionada.largo, null);
    }
    
    public void imprimirJuego (Graphics g){ 
        //Imprime la primera ficha en juego
        if(Inicio.imagen==1)
            g.drawImage(Inicio.imagen1.getImage(), Inicio.x, Inicio.y, Inicio.ancho, Inicio.largo, null);
        if(Inicio.imagen==2)
           g.drawImage(Inicio.imagen2.getImage(), Inicio.x, Inicio.y, Inicio.ancho, Inicio.largo, null);
        if(Inicio.imagen==3)
           g.drawImage(Inicio.imagen3.getImage(), Inicio.x, Inicio.y, Inicio.ancho, Inicio.largo, null);
        if(Inicio.imagen==4)
           g.drawImage(Inicio.imagen4.getImage(), Inicio.x, Inicio.y, Inicio.ancho, Inicio.largo, null);
        
        Ficha pos = Inicio.enlace2;
        
        //Si hay mas de una ficha en el juego
        if (Inicio != Fin)
            while(pos != null){
                if(pos.imagen==1)
                    g.drawImage(pos.imagen1.getImage(), pos.x, pos.y, pos.ancho, pos.largo, null);
                if(pos.imagen==2)
                   g.drawImage(pos.imagen2.getImage(), pos.x, pos.y, pos.ancho, pos.largo, null);
                if(pos.imagen==3)
                   g.drawImage(pos.imagen3.getImage(), pos.x, pos.y, pos.ancho, pos.largo, null);
                if(pos.imagen==4)
                   g.drawImage(pos.imagen4.getImage(), pos.x, pos.y, pos.ancho, pos.largo, null);
                
                if(pos.enlace3!=null){
                    Ficha pos2 = pos.enlace3;
                    while(pos2 != null){
                        if(pos2.imagen==1)
                           g.drawImage(pos2.imagen1.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==2)
                           g.drawImage(pos2.imagen2.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==3)
                           g.drawImage(pos2.imagen3.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==4)
                           g.drawImage(pos2.imagen4.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        
                        pos2 = pos2.enlace2;
                    }
                }
                
                if(pos.enlace4!=null){
                    Ficha pos2 = pos.enlace4;
                    while(pos2 != null){
                        if(pos2.imagen==1)
                           g.drawImage(pos2.imagen1.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==2)
                           g.drawImage(pos2.imagen2.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==3)
                           g.drawImage(pos2.imagen3.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==4)
                           g.drawImage(pos2.imagen4.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        
                        pos2 = pos2.enlace2;
                    }
                }

                pos = pos.enlace2;
            }
    }

    @Override
    public void keyPressed(KeyEvent e) { //Tecla de Espacio
        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
                p = login.usuario.InicioPartida;
                break;

            case KeyEvent.VK_DOWN:
                p = login.usuario.FinPartida;
                break;

            case KeyEvent.VK_LEFT:
                p = p.ant;
                break;

            case KeyEvent.VK_RIGHT:
                p = p.sig;
                break;
        }
    }
    
    @Override
    public void mouseClicked(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
