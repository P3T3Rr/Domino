
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;


public class Register extends javax.swing.JFrame {

    Login login;
    Metodos met;
    
    public Register(Login login,  Metodos met) {
        this.login = login;
        this.met = met;
        this.setTitle("Register");
        this.setResizable(false);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/descarga.png")).getImage());
        initComponents();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        Correo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        Contrasena = new javax.swing.JPasswordField();
        Nombre = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        cancelar = new javax.swing.JButton();
        aceptar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setLocation(new java.awt.Point(665, 242));
        setMaximumSize(new java.awt.Dimension(670, 435));
        setMinimumSize(new java.awt.Dimension(670, 435));
        setPreferredSize(new java.awt.Dimension(670, 435));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Magneto", 1, 48)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 204, 204));
        jLabel1.setText("Registro");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(200, 20, 250, 59);

        Correo.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Correo.setForeground(new java.awt.Color(0, 0, 0));
        Correo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        Correo.setToolTipText("correo");
        getContentPane().add(Correo);
        Correo.setBounds(290, 150, 280, 37);

        jLabel2.setFont(new java.awt.Font("Magneto", 1, 34)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(181, 28, 28));
        jLabel2.setText("Contraseña");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(30, 200, 210, 43);

        jLabel3.setFont(new java.awt.Font("Magneto", 1, 34)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(181, 28, 28));
        jLabel3.setText("Cédula");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(100, 150, 140, 43);

        Contrasena.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Contrasena.setForeground(new java.awt.Color(0, 0, 0));
        Contrasena.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        Contrasena.setToolTipText("contraseña");
        getContentPane().add(Contrasena);
        Contrasena.setBounds(290, 200, 280, 35);

        Nombre.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Nombre.setForeground(new java.awt.Color(0, 0, 0));
        Nombre.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        Nombre.setToolTipText("nombre");
        Nombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NombreActionPerformed(evt);
            }
        });
        getContentPane().add(Nombre);
        Nombre.setBounds(290, 100, 280, 37);

        jLabel4.setFont(new java.awt.Font("Magneto", 1, 34)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(181, 28, 28));
        jLabel4.setText("Nombre");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(80, 100, 160, 42);

        cancelar.setBackground(new java.awt.Color(0, 0, 0));
        cancelar.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        cancelar.setForeground(new java.awt.Color(181, 28, 28));
        cancelar.setText("Cancelar");
        cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelarActionPerformed(evt);
            }
        });
        getContentPane().add(cancelar);
        cancelar.setBounds(90, 280, 210, 61);

        aceptar.setBackground(new java.awt.Color(0, 0, 0));
        aceptar.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        aceptar.setForeground(new java.awt.Color(51, 255, 51));
        aceptar.setText("Aceptar");
        aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aceptarActionPerformed(evt);
            }
        });
        getContentPane().add(aceptar);
        aceptar.setBounds(370, 280, 200, 61);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fonde_Juego_opt.jpg"))); // NOI18N
        getContentPane().add(jLabel5);
        jLabel5.setBounds(0, 0, 680, 410);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_cancelarActionPerformed

    private void aceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aceptarActionPerformed
        String contrasena = "";
        char []  contra = Contrasena.getPassword();
        for(int i=0; i<contra.length; i++) contrasena += contra[i];

        if (Correo.getText().isEmpty() || "".equals(contrasena) || Nombre.getText().isEmpty())
        JOptionPane.showMessageDialog(null, "Campo Vacio...!!");
        else{
            boolean x = met.registrarse(Nombre.getText(), Correo.getText(), contrasena);
            if (x == true){
                JOptionPane.showMessageDialog(null, "Usuario creado...!!");
                this.dispose();
            }
            else JOptionPane.showMessageDialog(null, "Ya existe un usuario con la cedula " + Correo.getText() + "...!!");
        }
    }//GEN-LAST:event_aceptarActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        this.dispose();
    }//GEN-LAST:event_formWindowClosing

    private void NombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_NombreActionPerformed

    
    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField Contrasena;
    private javax.swing.JTextField Correo;
    private javax.swing.JTextField Nombre;
    private javax.swing.JButton aceptar;
    private javax.swing.JButton cancelar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    // End of variables declaration//GEN-END:variables
}
