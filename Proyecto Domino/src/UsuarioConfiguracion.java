
import javax.swing.ImageIcon;


public class UsuarioConfiguracion extends javax.swing.JFrame {
    Login login;
    Metodos met;
    
    public UsuarioConfiguracion(Login login,  Metodos met) {
        this.login = login;
        this.met = met;
        this.setTitle("Configuracion de Cuenta");
        this.setResizable(false);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/descarga.png")).getImage());
        initComponents();
        usuario.setText(String.valueOf(login.usuario.nombre));
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        usuario = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        NuevoJuego = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        salir = new javax.swing.JButton();
        atras3 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        nombre = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        cedula = new javax.swing.JTextField();
        contrasena = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        EditarUsuario = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        configurar = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setLocation(new java.awt.Point(400, 200));
        setMaximumSize(new java.awt.Dimension(1200, 700));
        setMinimumSize(new java.awt.Dimension(1200, 700));
        setPreferredSize(new java.awt.Dimension(1200, 700));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(null);

        jLabel3.setFont(new java.awt.Font("Magneto", 1, 48)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Main");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(20, 10, 140, 50);

        usuario.setFont(new java.awt.Font("Dialog", 3, 24)); // NOI18N
        usuario.setForeground(new java.awt.Color(255, 255, 255));
        usuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        usuario.setText("Nombre");
        usuario.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(usuario);
        usuario.setBounds(290, 10, 110, 40);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));
        jPanel1.setLayout(null);

        NuevoJuego.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        NuevoJuego.setText("Nuevo Juego");
        NuevoJuego.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        NuevoJuego.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NuevoJuegoActionPerformed(evt);
            }
        });
        jPanel1.add(NuevoJuego);
        NuevoJuego.setBounds(80, 0, 330, 70);

        jButton3.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        jButton3.setText("Partidas");
        jButton3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);
        jButton3.setBounds(80, 130, 330, 70);

        salir.setBackground(new java.awt.Color(204, 204, 204));
        salir.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/salir_opt.png"))); // NOI18N
        salir.setToolTipText("Salir");
        salir.setBorder(null);
        salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        jPanel1.add(salir);
        salir.setBounds(380, 260, 70, 70);

        atras3.setBackground(new java.awt.Color(204, 204, 204));
        atras3.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        atras3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cerrar_opt.png"))); // NOI18N
        atras3.setToolTipText("Cerrar Sesion");
        atras3.setBorder(null);
        atras3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        atras3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                atras3ActionPerformed(evt);
            }
        });
        jPanel1.add(atras3);
        atras3.setBounds(50, 260, 70, 70);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 240, 500, 440);

        jLabel5.setBackground(new java.awt.Color(76, 76, 76));
        jLabel5.setFont(new java.awt.Font("Magneto", 1, 55)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(204, 204, 204));
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/nuevo-gif-1-iloveimg-resized (1).gif"))); // NOI18N
        jLabel5.setText("Configuración");
        jLabel5.setIconTextGap(30);
        getContentPane().add(jLabel5);
        jLabel5.setBounds(610, 80, 500, 62);

        jLabel6.setFont(new java.awt.Font("Magneto", 1, 24)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(181, 28, 28));
        jLabel6.setText("Nuevo Nombre");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(630, 230, 210, 30);

        nombre.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        getContentPane().add(nombre);
        nombre.setBounds(890, 230, 173, 32);

        jLabel7.setFont(new java.awt.Font("Magneto", 1, 24)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(181, 28, 28));
        jLabel7.setText("Nueva Cédula");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(630, 290, 200, 30);

        cedula.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        getContentPane().add(cedula);
        cedula.setBounds(890, 290, 173, 32);

        contrasena.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        getContentPane().add(contrasena);
        contrasena.setBounds(890, 350, 173, 32);

        jLabel8.setFont(new java.awt.Font("Magneto", 1, 24)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(181, 28, 28));
        jLabel8.setText("Nueva Contraseña");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(630, 350, 240, 30);

        EditarUsuario.setBackground(new java.awt.Color(0, 0, 0));
        EditarUsuario.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        EditarUsuario.setForeground(new java.awt.Color(51, 255, 51));
        EditarUsuario.setText("Editar y Guardar ");
        EditarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EditarUsuarioActionPerformed(evt);
            }
        });
        getContentPane().add(EditarUsuario);
        EditarUsuario.setBounds(620, 450, 440, 61);
        getContentPane().add(jLabel4);
        jLabel4.setBounds(500, 0, 700, 680);

        configurar.setBackground(new java.awt.Color(255, 255, 255));
        configurar.setFont(new java.awt.Font("Dialog", 1, 36)); // NOI18N
        configurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/nuevo-gif-1-iloveimg-resized (1).gif"))); // NOI18N
        configurar.setToolTipText("Configurar Cuenta");
        configurar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        configurar.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        configurar.setIconTextGap(1);
        configurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                configurarActionPerformed(evt);
            }
        });
        getContentPane().add(configurar);
        configurar.setBounds(410, 10, 80, 70);

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fonde_Juego_opt.jpg"))); // NOI18N
        getContentPane().add(jLabel9);
        jLabel9.setBounds(500, 0, 700, 680);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/giphy.gif"))); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 0, 500, 280);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        atras();
    }//GEN-LAST:event_formWindowClosing

    private void EditarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EditarUsuarioActionPerformed
        met.editarCuenta(login.usuario.cedula, nombre.getText(), contrasena.getText(), cedula.getText());
    }//GEN-LAST:event_EditarUsuarioActionPerformed

    private void configurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_configurarActionPerformed
        UsuarioConfiguracion frame = new UsuarioConfiguracion(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_configurarActionPerformed

    private void NuevoJuegoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NuevoJuegoActionPerformed
        UsuarioNuevoJuego frame = new UsuarioNuevoJuego(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_NuevoJuegoActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        UsuarioPartidas frame = new UsuarioPartidas(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        this.dispose();
        login.dispose();
    }//GEN-LAST:event_salirActionPerformed

    private void atras3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_atras3ActionPerformed
        atras();
    }//GEN-LAST:event_atras3ActionPerformed
    
    public void atras(){
        login.setVisible(true);
        this.dispose();
    }
    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton EditarUsuario;
    private javax.swing.JButton NuevoJuego;
    private javax.swing.JButton atras3;
    private javax.swing.JTextField cedula;
    private javax.swing.JButton configurar;
    private javax.swing.JTextField contrasena;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField nombre;
    private javax.swing.JButton salir;
    private javax.swing.JLabel usuario;
    // End of variables declaration//GEN-END:variables
}
