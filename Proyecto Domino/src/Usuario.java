
import java.io.Serializable;


public class Usuario implements Serializable{
    String nombre;
    String cedula;
    String contrasena;
    Partida InicioPartida, FinPartida;
    
    Usuario sig, ant;
    
    public Usuario(String nombre, String cedula, String contrasena) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.contrasena = contrasena;
        this.sig = ant = null;
    }
    
    public void setPartida(Partida nuevo){
        if (InicioPartida == null){
            InicioPartida = FinPartida = nuevo;
        }
        else{
            InicioPartida.ant = nuevo;   
            nuevo.sig = InicioPartida;
            InicioPartida = nuevo;
        }
    }
}