import java.awt.AWTException;
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Juego3 extends Canvas implements MouseListener, KeyListener{
    Metodos met;
    Login login;
    
    Ficha seleccionada;
    
    ArrayList<Ficha> misFichas = new ArrayList<>();
    ArrayList<Ficha> fichasOponente = new ArrayList<>();
    ArrayList<Ficha> fichasOponente2 = new ArrayList<>();
    ArrayList<Ficha> fichasOponente3 = new ArrayList<>();
    
    Ficha Inicio;
    Ficha Fin;
    
    Calendar calendario = Calendar.getInstance();
    int hora =calendario.get(Calendar.HOUR_OF_DAY);
    int minuto = calendario.get(Calendar.MINUTE);
    int segundo = calendario.get(Calendar.SECOND);
    
    int JuegoYo;
    boolean trampa1 = true;
    boolean trampa2 = true;
    boolean trampa3 = true;
    boolean trampa4 = true;

    int Vuelta = 1;
    
    int PierdeTurno = 0;
    
    public Juego3(Metodos met, Login login) {
        this.login = login;
        this.met = met;
        addMouseListener(this);
        addKeyListener(this);
        agregarPrimerasFichas();
        quienVaPrimero();
    }
    
    public void paint(Graphics g) {
        super.paint(g);
        
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/fonde_Juego_opt - copia.jpg")).getImage(), 30, 170, 1630, 650, null);
        
        if (fichasOponente.isEmpty())
            JOptionPane.showMessageDialog(null, "YOU LOSE...!!");
        if (misFichas.isEmpty())
            JOptionPane.showMessageDialog(null, "YOU WIN...!!");
        if (fichasOponente2.isEmpty())
            JOptionPane.showMessageDialog(null, "YOU LOSE...!!");
        if (fichasOponente3.isEmpty())
            JOptionPane.showMessageDialog(null, "YOU LOSE...!!");
        
        if (JuegoYo == 1){
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Su turno.png")).getImage(), 900, 870, null);
            if (trampa1){
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Retire ficha.png")).getImage(), 1710, 300, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Un turno.png")).getImage(), 1710, 340, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Trampas.png")).getImage(), 1700, 253, null);
            }
        }
        if (JuegoYo == 2){
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Su turno.png")).getImage(), 550, 50, null);
            if (trampa2){
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Retire ficha.png")).getImage(), 1710, 300, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Un turno.png")).getImage(), 1710, 340, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Trampas.png")).getImage(), 1700, 253, null);
            }
        }
        if (JuegoYo == 3){
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Su turno2.png")).getImage(), 560, 50, null);
            if (trampa3){
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Retire ficha.png")).getImage(), 1710, 300, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Un turno.png")).getImage(), 1710, 340, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Trampas.png")).getImage(), 1700, 253, null);
            }
        }
        if (JuegoYo == 4){
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Su turno2.png")).getImage(), 1220, 50, null);
            if (trampa4){
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Retire ficha.png")).getImage(), 1710, 300, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Un turno.png")).getImage(), 1710, 340, null);
                g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Trampas.png")).getImage(), 1700, 253, null);
            }
        }
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Preciona Click Derecho.png")).getImage(), 30, 830, null);
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/pasar_opt.png")).getImage(), 1715, 830, null);
        g.drawImage(new ImageIcon(getClass().getResource("Imagenes/guardar_opt.png")).getImage(), 1580, 180, null);
        
        int x = 30;
        for (int i=0; i<misFichas.size(); i++){
            misFichas.get(i).x = x;
            misFichas.get(i).y = 865;
            x += 75;
            g.drawImage(misFichas.get(i).imagen1.getImage(), misFichas.get(i).x, misFichas.get(i).y, 72, 144, null);
        }
        
        x = 20;
        for (int i=0; i<fichasOponente.size(); i++){
            fichasOponente.get(i).x = x;
            fichasOponente.get(i).y = 10;
            x += 75;
            g.drawImage(fichasOponente.get(i).imagen1.getImage(), fichasOponente.get(i).x, fichasOponente.get(i).y, 72, 144, null);
        }
        
        x = 680;
        for (int i=0; i<fichasOponente2.size(); i++){
            fichasOponente2.get(i).x = x;
            fichasOponente2.get(i).y = 10;
            x += 75;
            g.drawImage(fichasOponente2.get(i).imagen1.getImage(), fichasOponente2.get(i).x, fichasOponente2.get(i).y, 72, 144, null);
        }
        
        x = 1350;
        for (int i=0; i<fichasOponente3.size(); i++){
            fichasOponente3.get(i).x = x;
            fichasOponente3.get(i).y = 10;
            x += 75;
            g.drawImage(fichasOponente3.get(i).imagen1.getImage(), fichasOponente3.get(i).x, fichasOponente3.get(i).y, 72, 144, null);
        }
        
        imprimirJuego(g);
        
        if(seleccionada!=null){
            if (Vuelta == 1 || Vuelta == 3){
                seleccionada.ancho = 72;
                seleccionada.largo = 144;
            }
            else{
                seleccionada.ancho = 144;
                seleccionada.largo = 72;
            }
            g.drawImage(new ImageIcon(getClass().getResource("Imagenes/Preciona Espacio.png")).getImage(), 1680, 500, null);
            imprimirSeleccionada(g);
        }
    }
    
    @Override
    public void mouseClicked(MouseEvent e) { 
        //Cuando preciona click izquierdo
        if (e.getButton() == MouseEvent.BUTTON1) { 
            // si preciona el boton de pasar de turno
            if ((e.getX() >= 1720) && (e.getX() <= 1820) && (e.getY() >= 870) && (e.getY() <= 960)){
                if (JuegoYo == 4)
                    JuegoYo = 0;
                JuegoYo+=1;
                repaint();
            }
            // si preciona el boton de guardar
            else if((e.getX() >= 1580) && (e.getX() <= 1650) && (e.getY() >= 180) && (e.getY() <= 250)){
                JOptionPane.showMessageDialog(null, "Partida Guardada...!!");
                try {
                    guardarPartida();
                } catch (IOException | AWTException ex) {
                    Logger.getLogger(Juego.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            // si preciona preciona el boton de trampa 1
            else if ((e.getX() >= 1710) && (e.getX() <= 1842) && (e.getY() >= 300) && (e.getY() <= 322)){
                System.out.println("Trampa Retire");
                if (seleccionada != null){
                    switch (JuegoYo) {
                        case 1:
                            seleccionada.trampa = 1;
                            trampa1 = false;
                            break;
                        case 2:
                            seleccionada.trampa = 1;
                            trampa2 = false;
                            break;
                        case 3:
                            seleccionada.trampa = 1;
                            trampa3 = false;
                            break;
                        default:
                            seleccionada.trampa = 1;
                            trampa4 = false;
                            break;
                    }
                    repaint();
                }
            }
            // si preciona preciona el boton de trampa 2
            else if ((e.getX() >= 1710) && (e.getX() <= 1842) && (e.getY() >= 340) && (e.getY() <= 362)){
                System.out.println("Trampa paso");
                if (seleccionada != null){
                    switch (JuegoYo) {
                        case 1:
                            seleccionada.trampa = 2;
                            trampa1 = false;
                            break;
                        case 2:
                            seleccionada.trampa = 2;
                            trampa2 = false;
                            break;
                        case 3:
                            seleccionada.trampa = 2;
                            trampa3 = false;
                            break;
                        default:
                            seleccionada.trampa = 2;
                            trampa4 = false;
                            break;
                    }
                    repaint();
                }
            }
            //si preciona cualquier otro lugar
            else{
                //si juego yo, recorra mi lista de fichas y vea si preciono ensima de una de ellas
                switch (JuegoYo) {
                    case 1:
                        for(int i=0; i<misFichas.size(); i++){
                            if ((e.getX() >= misFichas.get(i).x) && (e.getX() <= misFichas.get(i).x + 72) && (e.getY() >= misFichas.get(i).y) && (e.getY() <= misFichas.get(i).y + 144)) {
                                seleccionada = misFichas.get(i);
                                repaint();
                                return;
                            }
                        }
                        //si no juego yo, recorra la lista de fichas de mi oponente y vea si preciono ensima de una de ellas
                        break;
                    case 2:
                        for(int i=0; i<fichasOponente.size(); i++){
                            if ((e.getX() >= fichasOponente.get(i).x) && (e.getX() <= fichasOponente.get(i).x + 72) && (e.getY() >= fichasOponente.get(i).y) && (e.getY() <= fichasOponente.get(i).y + 144)) {
                                seleccionada = fichasOponente.get(i);
                                repaint();
                                return;
                            }
                        }   
                        break;
                    case 3:
                        for(int i=0; i<fichasOponente2.size(); i++){
                            if ((e.getX() >= fichasOponente2.get(i).x) && (e.getX() <= fichasOponente2.get(i).x + 72) && (e.getY() >= fichasOponente2.get(i).y) && (e.getY() <= fichasOponente2.get(i).y + 144)) {
                                seleccionada = fichasOponente2.get(i);
                                repaint();
                                return;
                            }
                        }   
                        break;
                    default:
                        for(int i=0; i<fichasOponente3.size(); i++){
                            if ((e.getX() >= fichasOponente3.get(i).x) && (e.getX() <= fichasOponente3.get(i).x + 72) && (e.getY() >= fichasOponente3.get(i).y) && (e.getY() <= fichasOponente3.get(i).y + 144)) {
                                seleccionada = fichasOponente3.get(i);
                                repaint();
                                return;
                            }
                        }   
                        break;
                }
                //si no, coloque la ficha seleccionada
                if (seleccionada != null){
                    agregar(e.getX(), e.getY());
                }    
            }
        }
    }
    
    @Override
    public void keyPressed(KeyEvent e) { //Tecla de Espacio
        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            if (Vuelta == 4)
                Vuelta = 0;
            Vuelta += 1;
            repaint();
        }
    } //Girar laimagen
    
    public void imprimirSeleccionada(Graphics g){ 
        if(Vuelta==1)
            g.drawImage(seleccionada.imagen1.getImage(), 1740, 590, seleccionada.ancho, seleccionada.largo, null);
        if(Vuelta==2)
            g.drawImage(seleccionada.imagen2.getImage(), 1704, 626, seleccionada.ancho, seleccionada.largo, null);
        if(Vuelta==3)
            g.drawImage(seleccionada.imagen3.getImage(), 1740, 590, seleccionada.ancho, seleccionada.largo, null);
        if(Vuelta==4)
            g.drawImage(seleccionada.imagen4.getImage(), 1704, 626, seleccionada.ancho, seleccionada.largo, null);
    } //Imprime la ficha seleccionada en una posicion especifica
    
    public void imprimirJuego (Graphics g){ 
        //Imprime la primera ficha en juego
        if(Inicio.imagen==1)
            g.drawImage(Inicio.imagen1.getImage(), Inicio.x, Inicio.y, Inicio.ancho, Inicio.largo, null);
        if(Inicio.imagen==2)
           g.drawImage(Inicio.imagen2.getImage(), Inicio.x, Inicio.y, Inicio.ancho, Inicio.largo, null);
        if(Inicio.imagen==3)
           g.drawImage(Inicio.imagen3.getImage(), Inicio.x, Inicio.y, Inicio.ancho, Inicio.largo, null);
        if(Inicio.imagen==4)
           g.drawImage(Inicio.imagen4.getImage(), Inicio.x, Inicio.y, Inicio.ancho, Inicio.largo, null);
        
        Ficha pos = Inicio.enlace2;
        
        //Si hay mas de una ficha en el juego
        if (Inicio != Fin)
            while(pos != null){
                if(pos.imagen==1)
                    g.drawImage(pos.imagen1.getImage(), pos.x, pos.y, pos.ancho, pos.largo, null);
                if(pos.imagen==2)
                   g.drawImage(pos.imagen2.getImage(), pos.x, pos.y, pos.ancho, pos.largo, null);
                if(pos.imagen==3)
                   g.drawImage(pos.imagen3.getImage(), pos.x, pos.y, pos.ancho, pos.largo, null);
                if(pos.imagen==4)
                   g.drawImage(pos.imagen4.getImage(), pos.x, pos.y, pos.ancho, pos.largo, null);
                
                if(pos.enlace3!=null){
                    Ficha pos2 = pos.enlace3;
                    while(pos2 != null){
                        if(pos2.imagen==1)
                           g.drawImage(pos2.imagen1.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==2)
                           g.drawImage(pos2.imagen2.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==3)
                           g.drawImage(pos2.imagen3.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==4)
                           g.drawImage(pos2.imagen4.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        
                        pos2 = pos2.enlace2;
                    }
                }
                
                if(pos.enlace4!=null){
                    Ficha pos2 = pos.enlace4;
                    while(pos2 != null){
                        if(pos2.imagen==1)
                           g.drawImage(pos2.imagen1.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==2)
                           g.drawImage(pos2.imagen2.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==3)
                           g.drawImage(pos2.imagen3.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        if(pos2.imagen==4)
                           g.drawImage(pos2.imagen4.getImage(), pos2.x, pos2.y, pos2.ancho, pos2.largo, null);
                        
                        pos2 = pos2.enlace2;
                    }
                }

                pos = pos.enlace2;
            }
    } //Imprime las fichas en juego
    
    public void agregarPrimerasFichas(){
        // Agregar mis 7 fichas
        for(int m=0; m<7; m++){
            int random = (int) (Math.random()*28+1);
            Ficha ficha = met.buscarFicha(random);
            while(ficha == null){
                random = (int) (Math.random()*28+1);
                ficha = met.buscarFicha(random);
            }
            misFichas.add(ficha);
            met.eliminarFicha(ficha);
        }
        // Agregar las 7 fichas de mi oponente
        for(int m=0; m<7; m++){
            int random = (int) (Math.random()*28+1);
            Ficha ficha = met.buscarFicha(random);
            while(ficha == null){
                random = (int) (Math.random()*28+1);
                ficha = met.buscarFicha(random);
            }
            fichasOponente.add(ficha);
            met.eliminarFicha(ficha);
        }
        
        for(int m=0; m<7; m++){
            int random = (int) (Math.random()*28+1);
            Ficha ficha = met.buscarFicha(random);
            while(ficha == null){
                random = (int) (Math.random()*28+1);
                ficha = met.buscarFicha(random);
            }
            fichasOponente2.add(ficha);
            met.eliminarFicha(ficha);
        }
        
        for(int m=0; m<7; m++){
            int random = (int) (Math.random()*28+1);
            Ficha ficha = met.buscarFicha(random);
            while(ficha == null){
                random = (int) (Math.random()*28+1);
                ficha = met.buscarFicha(random);
            }
            fichasOponente3.add(ficha);
            met.eliminarFicha(ficha);
        }
    } //Agregar las primeras 14 fichas
    
    public void quienVaPrimero(){
        int mayor = 6;
        while(true){
            for (int i=0; i<misFichas.size(); i++){
                if (misFichas.get(i).lado1 == mayor && misFichas.get(i).lado2 == mayor){
                    misFichas.get(i).x = 800;
                    misFichas.get(i).y = 450;
                    misFichas.get(i).imagen = 2;
                    misFichas.get(i).ancho = 144;
                    misFichas.get(i).largo = 72;
                    
                    Inicio = misFichas.get(i);
                    Fin = misFichas.get(i);
                    
                    misFichas.remove(misFichas.get(i));
                    JuegoYo = 2;
                    return;
                }
            }

            for (int i=0; i<fichasOponente.size(); i++){
                if (fichasOponente.get(i).lado1 == mayor && fichasOponente.get(i).lado2 == mayor){
                    fichasOponente.get(i).x = 800;
                    fichasOponente.get(i).y = 450;
                    fichasOponente.get(i).imagen = 2;
                    fichasOponente.get(i).ancho = 144;
                    fichasOponente.get(i).largo = 72;
                    
                    Inicio = fichasOponente.get(i);
                    Fin = fichasOponente.get(i);
                    
                    fichasOponente.remove(fichasOponente.get(i));
                    JuegoYo = 3;
                    return;
                }
            }
            
            for (int i=0; i<fichasOponente2.size(); i++){
                if (fichasOponente2.get(i).lado1 == mayor && fichasOponente2.get(i).lado2 == mayor){
                    fichasOponente2.get(i).x = 800;
                    fichasOponente2.get(i).y = 450;
                    fichasOponente2.get(i).imagen = 2;
                    fichasOponente2.get(i).ancho = 144;
                    fichasOponente2.get(i).largo = 72;
                    
                    Inicio = fichasOponente2.get(i);
                    Fin = fichasOponente2.get(i);
                    
                    fichasOponente2.remove(fichasOponente2.get(i));
                    JuegoYo = 4;
                    return;
                }
            }
            
            for (int i=0; i<fichasOponente3.size(); i++){
                if (fichasOponente3.get(i).lado1 == mayor && fichasOponente3.get(i).lado2 == mayor){
                    fichasOponente3.get(i).x = 800;
                    fichasOponente3.get(i).y = 450;
                    fichasOponente3.get(i).imagen = 2;
                    fichasOponente3.get(i).ancho = 144;
                    fichasOponente3.get(i).largo = 72;
                    
                    Inicio = fichasOponente3.get(i);
                    Fin = fichasOponente3.get(i);
                    
                    fichasOponente3.remove(fichasOponente3.get(i));
                    JuegoYo = 1;
                    return;
                }
            }
            mayor -= 1;
        }
    } //Decide quien pone la primera ficha
    
    public void agregar (int x, int y){
        if (seleccionada.lado1 != seleccionada.lado2){
            Ficha pos = Inicio;
            while (pos != null){ 
                if ((pos.lado1 == seleccionada.lado1) || (pos.lado1 == seleccionada.lado2) || (pos.lado2 == seleccionada.lado2) || (pos.lado2 == seleccionada.lado1)){
                    if (Inicio == Fin){
                        if ((x >= pos.x + 144) && (x <= pos.x + 144 + 72) && (y >= pos.y) && (y <= pos.y + 72)){
                            seleccionada.x = pos.x + 144;
                            seleccionada.y = pos.y;
                            SePuedeConectar(pos); return;
                            
                        }

                        if ((x >= pos.x ) && (x <= pos.x + 144) && (y >= pos.y - 72) && (y <= pos.y )){
                            if (seleccionada.largo != 72){
                                seleccionada.x = pos.x + 36;
                                seleccionada.y = pos.y - seleccionada.largo;
                                SePuedeConectar(pos); return;
                            }
                        }

                        if ((x >= pos.x ) && (x <= pos.x + 144) && (y >= pos.y + 72) && (y <= pos.y + 144 )){
                            if (seleccionada.largo != 72){
                                seleccionada.x = pos.x + 36;
                                seleccionada.y = pos.y + 72;
                                SePuedeConectar(pos); return;
                            }
                        }

                        if ((x >= pos.x - 72) && (x <= pos.x ) && (y >= pos.y ) && (y <= pos.y + 72 )){
                            seleccionada.x = pos.x - seleccionada.ancho;
                            seleccionada.y = pos.y;
                            SePuedeConectar(pos); return;
                        }
                    }
                    else{
                        if (pos != Fin && pos != Inicio && pos.lado1 != pos.lado2)
                            pos = pos.enlace2;
                        if (pos.largo == 72){
                            if (pos.lado1 == pos.lado2){
                                if ((x >= pos.x ) && (x <= pos.x + 144) && (y >= pos.y - 72) && (y <= pos.y )){
                                    if (seleccionada.largo != 72){
                                        seleccionada.x = pos.x + 36;
                                        seleccionada.y = pos.y - seleccionada.largo;
                                        SePuedeConectar(pos); return;
                                    }
                                }

                                if ((x >= pos.x ) && (x <= pos.x + 144) && (y >= pos.y + 72) && (y <= pos.y + 144 )){
                                    if (seleccionada.largo != 72){
                                        seleccionada.x = pos.x + 36;
                                        seleccionada.y = pos.y + 72;
                                        SePuedeConectar(pos); return;
                                    }
                                }
                            }
                            
                            if ((x >= pos.x + 144) && (x <= pos.x + 144 + 72) && (y >= pos.y) && (y <= pos.y + 72)){
                                seleccionada.x = pos.x + 144;
                                seleccionada.y = pos.y;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x + 72) && (x <= pos.x + 144) && (y >= pos.y + 72) && (y <= pos.y + 144)){
                                seleccionada.x = pos.x + 72;
                                seleccionada.y = pos.y + 72;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x + 72) && (x <= pos.x + 144) && (y >= pos.y - 72) && (y <= pos.y )){
                                seleccionada.x = pos.x + 72;
                                seleccionada.y = pos.y - seleccionada.largo;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x ) && (x <= pos.x + 72) && (y >= pos.y - 72) && (y <= pos.y )){
                                seleccionada.x = pos.x;
                                seleccionada.y = pos.y - seleccionada.largo;
                                SePuedeConectar(pos); return;
                            } 
                            if ((x >= pos.x ) && (x <= pos.x + 72) && (y >= pos.y + 72) && (y <= pos.y + 144 )){
                                seleccionada.x = pos.x;
                                seleccionada.y = pos.y + 72;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x - 72) && (x <= pos.x ) && (y >= pos.y ) && (y <= pos.y + 72 )){
                                seleccionada.x = pos.x - seleccionada.ancho;
                                seleccionada.y = pos.y;
                                SePuedeConectar(pos); return;
                            }
                        }
                        else{
                            if (pos.lado1 == pos.lado2){
                                if ((x >= pos.x - 72) && (x <= pos.x ) && (y >= pos.y ) && (y <= pos.y + 144 )){
                                    if (seleccionada.largo != 144){
                                        seleccionada.x = pos.x - seleccionada.ancho;
                                        seleccionada.y = pos.y + 36;
                                        SePuedeConectar(pos); return;
                                    }
                                }
                                if ((x >= pos.x + 72) && (x <= pos.x + 144) && (y >= pos.y ) && (y <= pos.y + 144 )){
                                    if (seleccionada.largo != 144){
                                        seleccionada.x = pos.x + 72;
                                        seleccionada.y = pos.y + 36;
                                        SePuedeConectar(pos); return;
                                    }
                                }
                            }
                            
                            if ((x >= pos.x + 72) && (x <= pos.x + 144 ) && (y >= pos.y) && (y <= pos.y + 72)){
                                seleccionada.x = pos.x + 72;
                                seleccionada.y = pos.y;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x + 72) && (x <= pos.x + 144) && (y >= pos.y + 72) && (y <= pos.y + 144)){
                                seleccionada.x = pos.x + 72;
                                seleccionada.y = pos.y + 72;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x ) && (x <= pos.x + 72) && (y >= pos.y + 144) && (y <= pos.y + 144 + 72)){
                                seleccionada.x = pos.x;
                                seleccionada.y = pos.y + 144;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x - 72) && (x <= pos.x ) && (y >= pos.y + 72) && (y <= pos.y + 144)){
                                seleccionada.x = pos.x - seleccionada.ancho;
                                seleccionada.y = pos.y + 72;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x - 72) && (x <= pos.x ) && (y >= pos.y ) && (y <= pos.y + 72)){
                                seleccionada.x = pos.x - seleccionada.ancho;
                                seleccionada.y = pos.y;
                                SePuedeConectar(pos); return;
                            }
                            if ((x >= pos.x ) && (x <= pos.x + 72 ) && (y >= pos.y - 72) && (y <= pos.y )){
                                seleccionada.x = pos.x;
                                seleccionada.y = pos.y - seleccionada.largo;
                                SePuedeConectar(pos); return;
                            }
                        }
                    }
                }
                pos = pos.enlace2;
            }  
        }
        else{
            Ficha pos = Inicio;
            while (pos != null){
                if (pos.lado1!=pos.lado2 && pos!=Inicio && pos!= Fin)
                    pos = pos.enlace2;
                if ((pos.lado1 == seleccionada.lado1) || (pos.lado1 == seleccionada.lado2) || (pos.lado2 == seleccionada.lado2) || (pos.lado2 == seleccionada.lado1)){
                    if (pos.largo == 72){
                        if ((x >= pos.x + 144) && (x <= pos.x + 144 + 72) && (y >= pos.y) && (y <= pos.y + 72)){
                            if (seleccionada.largo == 72){
                                seleccionada.x = pos.x + 144;
                                seleccionada.y = pos.y; return;
                            }
                            else{
                                seleccionada.x = pos.x + 144;
                                seleccionada.y = pos.y - 36; 
                            }
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x - 72) && (x <= pos.x ) && (y >= pos.y ) && (y <= pos.y + 72 )){
                            if (seleccionada.largo == 72){
                                seleccionada.x = pos.x - seleccionada.ancho;
                                seleccionada.y = pos.y;
                            }
                            else{
                                seleccionada.x = pos.x - seleccionada.ancho;
                                seleccionada.y = pos.y - 36;
                            }
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x + 72) && (x <= pos.x + 144) && (y >= pos.y + 72) && (y <= pos.y + 144)){
                            seleccionada.x = pos.x + 72;
                            seleccionada.y = pos.y + 72;
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x + 72) && (x <= pos.x + 144) && (y >= pos.y - 72) && (y <= pos.y )){
                            seleccionada.x = pos.x + 72;
                            seleccionada.y = pos.y - seleccionada.largo;
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x ) && (x <= pos.x + 72) && (y >= pos.y - 72) && (y <= pos.y )){
                            seleccionada.x = pos.x;
                            seleccionada.y = pos.y - seleccionada.largo;
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x ) && (x <= pos.x + 72) && (y >= pos.y + 72) && (y <= pos.y + 144 )){
                            seleccionada.x = pos.x;
                            seleccionada.y = pos.y + 72;
                            SePuedeConectar(pos); return;
                        }
                    }
                    else{
                        if ((x >= pos.x ) && (x <= pos.x + 72) && (y >= pos.y + 144) && (y <= pos.y + 144 + 72)){
                            if (seleccionada.largo == 144){
                                seleccionada.x = pos.x;
                                seleccionada.y = pos.y + 144;
                            }
                            else{
                                seleccionada.x = pos.x-36;
                                seleccionada.y = pos.y + 144;
                            }
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x ) && (x <= pos.x + 72 ) && (y >= pos.y - 72) && (y <= pos.y )){
                            if (seleccionada.largo == 144){
                                seleccionada.x = pos.x;
                                seleccionada.y = pos.y - seleccionada.largo;
                            }
                            else{
                                seleccionada.x = pos.x-36;
                                seleccionada.y = pos.y - seleccionada.largo;
                            }
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x + 72) && (x <= pos.x + 144 ) && (y >= pos.y) && (y <= pos.y + 72)){
                            seleccionada.x = pos.x + 72;
                            seleccionada.y = pos.y;
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x + 72) && (x <= pos.x + 144) && (y >= pos.y + 72) && (y <= pos.y + 144)){
                            seleccionada.x = pos.x + 72;
                            seleccionada.y = pos.y + 72;
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x - 72) && (x <= pos.x ) && (y >= pos.y + 72) && (y <= pos.y + 144)){
                            seleccionada.x = pos.x - seleccionada.ancho;
                            seleccionada.y = pos.y + 72;
                            SePuedeConectar(pos); return;
                        }
                        if ((x >= pos.x - 72) && (x <= pos.x ) && (y >= pos.y ) && (y <= pos.y + 72)){
                            seleccionada.x = pos.x - seleccionada.ancho;
                            seleccionada.y = pos.y;
                            SePuedeConectar(pos); return;
                        }
                    }
                }
                pos = pos.enlace2;
            }
        }
        repaint();
    } //Se pregunta donde quiere colocar la ficha
    
    public void SePuedeConectar (Ficha ficha){
        seleccionada.imagen = Vuelta;
        
        if (ficha.lado1 == ficha.lado2){
            if (ficha == Inicio){
                seleccionada.enlace2 = Inicio;
                Inicio.enlace1 = seleccionada;
                Inicio = seleccionada;
                switch (JuegoYo) {
                    case 1:
                        misFichas.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    case 2:
                        fichasOponente.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    case 3:
                        fichasOponente2.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    default:
                        fichasOponente3.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                }
                seleccionada = null;
                repaint();
            }

            else if (ficha == Fin){
                seleccionada.enlace1 = Fin;
                Fin.enlace2 = seleccionada;
                Fin = seleccionada;
                switch (JuegoYo) {
                    case 1:
                        misFichas.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    case 2:
                        fichasOponente.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    case 3:
                        fichasOponente2.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    default:
                        fichasOponente3.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                }
                seleccionada = null;
                repaint();
            }
            else if(ficha.enlace1 != null && ficha.enlace2 != null && ficha.enlace3 == null){
                seleccionada.enlace1 = ficha;
                ficha.enlace3 = seleccionada;
                switch (JuegoYo) {
                    case 1:
                        misFichas.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    case 2:
                        fichasOponente.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    case 3:
                        fichasOponente2.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    default:
                        fichasOponente3.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                }
                seleccionada = null;
            }
            else if(ficha.enlace1 != null && ficha.enlace2 != null && ficha.enlace3 != null && ficha.enlace4 == null){
                seleccionada.enlace1 = ficha;
                ficha.enlace4 = seleccionada;
                switch (JuegoYo) {
                    case 1:
                        misFichas.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    case 2:
                        fichasOponente.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    case 3:
                        fichasOponente2.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    default:
                        fichasOponente3.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                }
                seleccionada = null;
            }
        }
        else{
            if (ficha == Inicio){
                seleccionada.enlace2 = Inicio;
                Inicio.enlace1 = seleccionada;
                Inicio = seleccionada;
                switch (JuegoYo) {
                    case 1:
                        misFichas.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    case 2:
                        fichasOponente.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    case 3:
                        fichasOponente2.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    default:
                        fichasOponente3.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                }
                seleccionada = null;
                repaint();
            }

            else if (ficha == Fin){
                seleccionada.enlace1 = Fin;
                Fin.enlace2 = seleccionada;
                Fin = seleccionada;
                switch (JuegoYo) {
                    case 1:
                        misFichas.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    case 2:
                        fichasOponente.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    case 3:
                        fichasOponente2.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                    default:
                        fichasOponente3.remove(seleccionada);
                        if (PierdeTurno != 3){
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if(PierdeTurno != 0)
                                PierdeTurno += 1;
                                
                        }
                        else{
                            PierdeTurno = 0;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                            if (JuegoYo == 4)
                                JuegoYo = 0;
                            JuegoYo+=1;
                        }
                        break;
                }
                seleccionada = null;
                repaint();
            }
        }
        
        if (ficha.enlace1 != null && ficha.enlace2 != null && Inicio != Fin){
            
            if (ficha.trampa == 2){ //trampa de pierde turno
                PierdeTurno = 1;
                JOptionPane.showMessageDialog(null, "Trampa activada...!! A perdido un turno");
            }
            if(ficha.trampa == 1){
                JOptionPane.showMessageDialog(null, "Trampa activada...!! se le a retirado la ficha");
            }
        }
        repaint();
    } //Se pregunta en que extremo de la lista se va a colocar la ficha

    public void guardarPartida() throws IOException, AWTException{
       
        //Image capturas = new ImageIcon(getClass().getResource("src/Capturas/Captura"+hora+minuto+segundo + ".jpg")).getImage();
       
        Partida nueva = new Partida( seleccionada, misFichas, fichasOponente, fichasOponente2, fichasOponente3, Inicio, Fin, JuegoYo, trampa1, trampa2, trampa3, trampa4, Vuelta);
        this.login.usuario.setPartida(nueva);
        met.guardar();
    }
    
    @Override
    public void mousePressed(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
