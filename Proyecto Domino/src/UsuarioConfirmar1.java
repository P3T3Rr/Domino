
import javax.swing.ImageIcon;
import javax.swing.JFrame;


public class UsuarioConfirmar1 extends javax.swing.JFrame {
    Login login;
    Metodos met;
    UsuarioNuevoJuego NJ;
    
    public UsuarioConfirmar1(Login login,  Metodos met, UsuarioNuevoJuego NJ) {
        this.login = login;
        this.met = met;
        this.NJ = NJ;
        this.setTitle("Contraseña del Oponente");
        this.setResizable(false);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/descarga.png")).getImage());
        initComponents();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        Contrasena = new javax.swing.JPasswordField();
        jLabel1 = new javax.swing.JLabel();
        Jugar = new javax.swing.JButton();
        Jugar1 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();

        jLabel2.setText("jLabel2");

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setLocation(new java.awt.Point(710, 280));
        setMaximumSize(new java.awt.Dimension(570, 400));
        setMinimumSize(new java.awt.Dimension(570, 400));
        setPreferredSize(new java.awt.Dimension(570, 400));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(null);

        Contrasena.setBackground(new java.awt.Color(51, 51, 51));
        Contrasena.setFont(new java.awt.Font("Consolas", 0, 24)); // NOI18N
        Contrasena.setForeground(new java.awt.Color(204, 204, 204));
        Contrasena.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        Contrasena.setText("444");
        Contrasena.setToolTipText("contraseña");
        getContentPane().add(Contrasena);
        Contrasena.setBounds(180, 170, 190, 40);

        jLabel1.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(204, 204, 204));
        jLabel1.setText("Contraseña del Oponente");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(30, 80, 500, 60);

        Jugar.setBackground(new java.awt.Color(0, 0, 0));
        Jugar.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        Jugar.setForeground(new java.awt.Color(51, 255, 51));
        Jugar.setText("Aceptar");
        Jugar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JugarActionPerformed(evt);
            }
        });
        getContentPane().add(Jugar);
        Jugar.setBounds(290, 280, 210, 60);

        Jugar1.setBackground(new java.awt.Color(0, 0, 0));
        Jugar1.setFont(new java.awt.Font("Magneto", 1, 36)); // NOI18N
        Jugar1.setForeground(new java.awt.Color(166, 31, 31));
        Jugar1.setText("Cancelar");
        Jugar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jugar1ActionPerformed(evt);
            }
        });
        getContentPane().add(Jugar1);
        Jugar1.setBounds(60, 280, 210, 60);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fonde_Juego_opt - copia.jpg"))); // NOI18N
        jLabel4.setMaximumSize(new java.awt.Dimension(600, 500));
        jLabel4.setMinimumSize(new java.awt.Dimension(600, 500));
        jLabel4.setPreferredSize(new java.awt.Dimension(600, 500));
        getContentPane().add(jLabel4);
        jLabel4.setBounds(0, 0, 560, 410);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
         this.dispose();
    }//GEN-LAST:event_formWindowClosing

    private void JugarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JugarActionPerformed
        String contrasena = "";
        char []  contra = Contrasena.getPassword();
        for(int i=0; i<contra.length; i++) contrasena += contra[i];
            boolean x = met.InicioDeSesion(NJ.OponentesSeleccionados.get(0).cedula, contrasena);
            if (x == true){
                this.dispose();
                met.CrearFichas();
                Juego jugar = new Juego(met, login);
                
                JFrame frameJuego = new JFrame();
                frameJuego.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                frameJuego.setLocation(10, 10);
                frameJuego.setSize(1900,1050);
                frameJuego.setTitle("Juego 1 vs 1");
                frameJuego.setResizable(false);
                frameJuego.add(jugar);
                frameJuego.setIconImage(new ImageIcon(getClass().getResource("/Imagenes/descarga.png")).getImage());
                frameJuego.setVisible(true);
            }
    }//GEN-LAST:event_JugarActionPerformed

    private void Jugar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Jugar1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_Jugar1ActionPerformed

    /**
     * @param args the command line arguments
     */
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField Contrasena;
    private javax.swing.JButton Jugar;
    private javax.swing.JButton Jugar1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    // End of variables declaration//GEN-END:variables
}
