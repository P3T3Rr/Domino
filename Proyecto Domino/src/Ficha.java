    
import java.io.Serializable;
import javax.swing.ImageIcon;

public class Ficha implements Serializable{
   int id;
   int lado1;
   int lado2;
   Ficha enlace1 = null;
   Ficha enlace2 = null;
   Ficha enlace3 = null;
   Ficha enlace4 = null;
   public int x;
   public int y;
   int ancho;
   int largo;
   public int imagen;
   ImageIcon imagen1, imagen2, imagen3, imagen4;
   Ficha sig;
   int trampa;
   

    public Ficha(int id, int lado1, int lado2, int x, int y, ImageIcon imagen1, ImageIcon imagen2, ImageIcon imagen3, ImageIcon imagen4) {
        this.id = id;
        this.lado1 = lado1;
        this.lado2 = lado2;
        this.x = x;
        this.y = y;
        this.imagen1 = imagen1;
        this.imagen2 = imagen2;
        this.imagen3 = imagen3;
        this.imagen4 = imagen4;
        this.ancho = 72;
        this.largo = 144;
        this.imagen = 1;
        this.trampa = 0;
    }
}
