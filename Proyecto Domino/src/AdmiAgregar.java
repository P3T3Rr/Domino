
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;


public class AdmiAgregar extends javax.swing.JFrame {
    
    Login login;
    Metodos met;
    
    public AdmiAgregar(Login login,  Metodos met) {
        this.login = login;
        this.met = met;
        this.setTitle("Administrador");
        this.setResizable(false);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/descarga.png")).getImage());
        initComponents();
    }

 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel5 = new javax.swing.JLabel();
        Agregar = new javax.swing.JButton();
        Editar = new javax.swing.JButton();
        Eliminar = new javax.swing.JButton();
        Ver = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        atras = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        nombre = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        cedula = new javax.swing.JTextField();
        contraseña = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        AgregarUsuario = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setBackground(new java.awt.Color(102, 102, 102));
        setLocation(new java.awt.Point(500, 200));
        setMaximizedBounds(new java.awt.Rectangle(932, 634, 634, 634));
        setMaximumSize(new java.awt.Dimension(900, 590));
        setMinimumSize(new java.awt.Dimension(900, 590));
        setPreferredSize(new java.awt.Dimension(900, 590));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(null);

        jLabel5.setBackground(new java.awt.Color(0, 0, 0));
        jLabel5.setFont(new java.awt.Font("Dialog", 1, 20)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Jeremmy Soto");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(130, 100, 150, 30);

        Agregar.setBackground(new java.awt.Color(204, 204, 204));
        Agregar.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        Agregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/agregar usuario_opt.png"))); // NOI18N
        Agregar.setText("Agregar Usuario");
        Agregar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Agregar.setBorderPainted(false);
        Agregar.setIconTextGap(10);
        Agregar.setOpaque(false);
        Agregar.setSelected(true);
        Agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarActionPerformed(evt);
            }
        });
        getContentPane().add(Agregar);
        Agregar.setBounds(-10, 180, 280, 60);

        Editar.setBackground(new java.awt.Color(204, 204, 204));
        Editar.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        Editar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/editar usuario_opt.png"))); // NOI18N
        Editar.setText("Editar Usuario");
        Editar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Editar.setBorderPainted(false);
        Editar.setIconTextGap(15);
        Editar.setOpaque(false);
        Editar.setSelected(true);
        Editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EditarActionPerformed(evt);
            }
        });
        getContentPane().add(Editar);
        Editar.setBounds(-20, 250, 290, 60);

        Eliminar.setBackground(new java.awt.Color(204, 204, 204));
        Eliminar.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        Eliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/elinimar usuario_opt.png"))); // NOI18N
        Eliminar.setText("Eliminar Usuario");
        Eliminar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Eliminar.setBorderPainted(false);
        Eliminar.setIconTextGap(10);
        Eliminar.setOpaque(false);
        Eliminar.setSelected(true);
        Eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EliminarActionPerformed(evt);
            }
        });
        getContentPane().add(Eliminar);
        Eliminar.setBounds(-10, 320, 280, 60);

        Ver.setBackground(new java.awt.Color(102, 102, 102));
        Ver.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        Ver.setForeground(new java.awt.Color(243, 243, 243));
        Ver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ver usuarios_opt.png"))); // NOI18N
        Ver.setText("Ver Usuarios");
        Ver.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Ver.setBorderPainted(false);
        Ver.setIconTextGap(20);
        Ver.setOpaque(false);
        Ver.setSelected(true);
        Ver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VerActionPerformed(evt);
            }
        });
        getContentPane().add(Ver);
        Ver.setBounds(-10, 440, 280, 80);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/foto de perfil_opt (1).png"))); // NOI18N
        getContentPane().add(jLabel3);
        jLabel3.setBounds(30, 70, 60, 60);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/foto de portada_opt (2).png"))); // NOI18N
        getContentPane().add(jLabel4);
        jLabel4.setBounds(-20, 0, 300, 100);

        jLabel1.setBackground(new java.awt.Color(204, 204, 204));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fonde_Juego_opt.jpg"))); // NOI18N
        jLabel1.setText("Jeremmy");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 280, 550);

        jPanel1.setBackground(new java.awt.Color(119, 119, 119));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setBackground(new java.awt.Color(76, 76, 76));
        jLabel2.setFont(new java.awt.Font("Dialog", 1, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/agregar usuario_opt.png"))); // NOI18N
        jLabel2.setText("Agregar Usuario");
        jLabel2.setIconTextGap(30);

        atras.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        atras.setToolTipText("Salir");
        atras.setBorder(null);
        atras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                atrasActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel6.setText("Nombre");

        nombre.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel7.setText("Cedula");

        cedula.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N

        contraseña.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N

        jLabel8.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel8.setText("Contraseña");

        AgregarUsuario.setBackground(new java.awt.Color(51, 255, 51));
        AgregarUsuario.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        AgregarUsuario.setText("Agregar Usuario");
        AgregarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarUsuarioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(72, 72, 72)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(45, 45, 45)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel8))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 59, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(cedula)
                                        .addComponent(contraseña, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(nombre, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(AgregarUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(atras, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(17, 17, 17))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(atras)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addGap(59, 59, 59)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(31, 31, 31)
                        .addComponent(jLabel7)
                        .addGap(27, 27, 27)
                        .addComponent(jLabel8))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(nombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(cedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25)
                        .addComponent(contraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(40, 40, 40)
                .addComponent(AgregarUsuario)
                .addContainerGap(159, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);
        jPanel1.setBounds(280, 0, 610, 550);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        login.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_formWindowClosing

    private void AgregarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarUsuarioActionPerformed
        boolean x = met.registrarse(nombre.getText(), cedula.getText(), contraseña.getText());
        if (x == true){
            JOptionPane.showMessageDialog(null, "Usuario creado...!!");
        }
        else JOptionPane.showMessageDialog(null, "Ya existe un usuario con la cedula " + cedula.getText() + "...!!");

    }//GEN-LAST:event_AgregarUsuarioActionPerformed

    private void atrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_atrasActionPerformed
        login.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_atrasActionPerformed

    private void AgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarActionPerformed
        AdmiAgregar frame = new AdmiAgregar(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_AgregarActionPerformed

    private void EditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EditarActionPerformed
        AdmiEditar frame = new AdmiEditar(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_EditarActionPerformed

    private void EliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EliminarActionPerformed
        AdmiEliminar frame = new AdmiEliminar(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_EliminarActionPerformed

    private void VerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VerActionPerformed
        AdmiVer frame = new AdmiVer(login, met);
        frame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_VerActionPerformed

    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Agregar;
    private javax.swing.JButton AgregarUsuario;
    private javax.swing.JButton Editar;
    private javax.swing.JButton Eliminar;
    private javax.swing.JButton Ver;
    private javax.swing.JButton atras;
    private javax.swing.JTextField cedula;
    private javax.swing.JTextField contraseña;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField nombre;
    // End of variables declaration//GEN-END:variables
}
